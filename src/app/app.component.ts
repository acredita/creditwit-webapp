
import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { LoginPage } from './pages/login/login.page';
import { StorageService } from './services/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private screenOrientation: ScreenOrientation,
    private deeplinks: Deeplinks,
    private navController: NavController,
    private storage: StorageService,
    private route: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString('#106EB0');
      this.splashScreen.hide();
      if ( this.platform.is('cordova') ){
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }
      this.setupDeepLinks();
    });
  }

  setupDeepLinks(){
    this.deeplinks.routeWithNavController(this.navController, {
      '/login': LoginPage,
     // e.g '/products/:productId': ProductPage
    }).subscribe(match => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data
        //alert( JSON.stringify( match ) );

        let args = match.$args;

        let data = {
          modulo: args.m,
          id: args.d
        }
        this.storage.setPath( JSON.stringify( data ) );

        this.route.navigateByUrl( `/login?d=${data.id}&m=${data.modulo}` );
        

        console.log('Successfully matched route', match);
      }, nomatch => {
        // nomatch.$link - the full link data
        //alert( JSON.stringify( nomatch ) );
        console.error('Got a deeplink that didn\'t match', nomatch);
      });
  }
}
