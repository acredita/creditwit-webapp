import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { CurrencyInputComponent } from './currency-input/currency-input.component';



@NgModule({
  declarations: [
    HeaderComponent,
    CurrencyInputComponent
  ],
  exports: [
    HeaderComponent,
    CurrencyInputComponent
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
  ]
})
export class ComponentsModule { }
