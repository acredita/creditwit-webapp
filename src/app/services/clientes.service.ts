import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  // url: string = "https://testapi.creditwit.app/access"; //Desarrollo
  url: string = "https://api.creditwit.app/access"; //Produccion

  constructor(private http: HttpClient,
    private storage: StorageService) { }


  getCedula(data) {
    return this.http.get(this.url + '/clientes/check/' + data);
  }

  registro(data) {
    return this.http.put(this.url + '/clientes/registro', data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  confirmClient(data) {
    return this.http.post(this.url + '/clientes/confirma', data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getRefreshToken(),
      },
    });
  }

  resetConfirmClient() {
    return this.http.get(this.url + '/clientes/confirma', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getRefreshToken(),
      },
    });
  }

}
