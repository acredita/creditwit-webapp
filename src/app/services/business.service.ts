import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { JwtHelperService } from "@auth0/angular-jwt";
import { AuthService } from './auth.service';
import { NavController } from '@ionic/angular';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  // url: string = "https://testapi.creditwit.app/business"; //Desarrollo
  url: string = "https://api.creditwit.app/business"; //Produccion

  constructor(private http: HttpClient,
    private storage: StorageService,
    private authService: AuthService,
    private navCtrl: NavController) { }


  async refreshToken() {
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(this.storage.getToken());
    if (isExpired) {
      try {
        let token: any = await this.authService.refreshToken().toPromise();
        console.log(token)
        this.storage.setToken(token.accessToken);
      }
      catch (err) {
        localStorage.clear();
        Swal.fire({
          confirmButtonColor: '#E7A016',
          text: 'Sesión expiró por seguridad',
          heightAuto: false,
          showCloseButton: true,
          confirmButtonText: 'Aceptar',
        });
        this.navCtrl.navigateRoot('');
      }
    }
  }
  /** DATA DE USUARIO */
  async getPerfil() {
    await this.refreshToken();
    return this.http.get(this.url + '/clientes/perfil', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async getRatingHistory() {
    await this.refreshToken();
    return this.http.get(this.url + '/rating/historial', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  // CUENTAS BANCARIAS
  async addcuentasBancarias(data) {
    await this.refreshToken();
    return this.http.put(this.url + '/clientes/cuentasBancarias', data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async modifycuentasBancarias(data) {
    await this.refreshToken();
    return this.http.patch(this.url + '/clientes/cuentasBancarias', data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async deletecuentasBancarias(data) {
    await this.refreshToken();
    return this.http.delete(this.url + '/clientes/cuentasBancarias/' + data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async typeClient(data) {
    await this.refreshToken();
    return this.http.patch(this.url + '/clientes/tipo', data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  /** DEUDAS */
  async getDeudor(data) { // se llama para obtener los datos del deudor antes de registrar la deuda 
    await this.refreshToken();
    return this.http.get(this.url + '/deudas/deudor/' + data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      }
    });
  }

  async registrarDeuda(data) {
    await this.refreshToken();
    // se llama una vez registrado los plazos y demas detalles de la deuda
    return this.http.post(this.url + '/deudas/registrar', data, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }

  /** MODULO PENDIENTES */

  async getPendientes() {
    await this.refreshToken();
    return this.http.get(this.url + '/deudas/pendientes', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async answerPayment(data) {
    await this.refreshToken();
    // PARA EL ACREEDOR
    // obtengo el id de la deuda y si acepta o no
    return this.http.post(this.url + '/deudas/aceptarPago', data, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }

  async answerDoubt(data) {
    await this.refreshToken();
    // PARA EL DEUDOR
    // obtengo el id de la deuda y si acepta o no
    return this.http.post(this.url + '/deudas/aceptar', data, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }

  async getDoubt(data) {
    await this.refreshToken();
    return this.http.get(this.url + '/deudas/consultar/' + data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async rechazarDeuda(data) {
    await this.refreshToken();
    return this.http.patch(this.url + '/deudas/reconocerRechazo/' + data, null, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async registrarPago(data) {
    await this.refreshToken();
    return this.http.post(this.url + '/pagos/registrar', data, {
      headers: { 'Authorization': this.storage.getToken(), },
    });
  }

  async getCuentasAcreedor(data) {
    await this.refreshToken();
    return this.http.get(this.url + '/clientes/cuentasBancarias/' + data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    });
  }

  async markDoubtAs(deuda, data) {
    await this.refreshToken();
    return this.http.post(this.url + '/deudas/marcar/' + deuda, data, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }

  async confirmPayment(payment, response) {
    await this.refreshToken();
    // confirma o rechaza pago
    return this.http.post(this.url + '/pagos/reconocer/' + payment, response, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }


  async doubtRefusedAcknowledge(doubt) {
    await this.refreshToken();
    // Reconocimiento de rechazo de deuda
    return this.http.get(this.url + '/pagos/reconocerRechazo/' + doubt, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }

  async calificaDeudor(data) {
    await this.refreshToken();
    return this.http.post(this.url + '/rating/crear', data, {
      headers: { 'Content-Type': 'application/json', 'Authorization': this.storage.getToken(), },
    });
  }

  async getHistorial(data) {
    await this.refreshToken();
    return this.http.get(this.url + '/pagos/historial', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
      params: {
        'salta': data.salta,
        'limite': data.limite
      }
    });
  }

  async getHistorialFiltro(data) {
    await this.refreshToken();
    return this.http.get(this.url + '/pagos/historialFiltro', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
      params: {
        'desde': data.desde,
        'hasta': data.hasta
      }
    });
  }

  async setTokenPush(data) {
    await this.refreshToken();
    return this.http.patch(this.url + '/clientes/addFirebaseToken', data, {
      headers: { 'Authorization': this.storage.getToken(), },
    });
  }

  async activePush() {
    await this.refreshToken();
    return this.http.get(this.url + '/clientes/changePush', {
      headers: { 'Authorization': this.storage.getToken(), },
    });
  }

  getCuentas() {
    return this.http.get<any[]>('/assets/data/lista_Bancos.json');
  }

  async getDirectory() {
    await this.refreshToken();
    return this.http.get(this.url + '/clientes/directorio', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken(),
      },
    }).toPromise();
  }

}
