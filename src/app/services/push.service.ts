import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { BusinessService } from './business.service';
import { StorageService } from './storage.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PushService {

  initOneSignal: boolean = false;

  constructor( private oneSignal: OneSignal,
    private businessService: BusinessService,
    private storage: StorageService,
    private navCtrl: NavController) { }

  configuracionInicial(){
    this.oneSignal.startInit('ac17bdec-8c93-4b19-bed9-616bd3aedf13', '412314785109');
    
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.getPermissionSubscriptionState().then( async info => {
      console.log(info);
      let userData = JSON.parse(this.storage.getUserData());
      if ( info.permissionStatus.status == 1 ){
        if ( userData.config.pushNotifications ){
          (await this.businessService.activePush())
          .subscribe( (resp: any) => {
            console.log(resp)
            userData.config.pushNotifications = resp.pushNotifications;
            this.storage.setUserData( JSON.stringify( userData ) );
          }, (err) => {
            console.log(err);
          });
        }
      }
    }, (err) => {
      console.log(err)
    })
    if (!this.initOneSignal){
      this.oneSignal.addPermissionObserver().subscribe(async ( permiso ) => {
        console.log(permiso);
        let userData = JSON.parse(this.storage.getUserData());
        if ( permiso.to.status == 1 ){
          if ( userData.config.pushNotifications ){
            (await this.businessService.activePush())
            .subscribe( (resp: any) => {
              console.log(resp)
              userData.config.pushNotifications = resp.pushNotifications;
              this.storage.setUserData( JSON.stringify( userData ) );
            }, (err) => {
              console.log(err);
            });
          }
        }
      }, (err) => {
        console.log(err);
      });
    }
    this.initOneSignal = true;
    this.oneSignal.handleNotificationOpened().subscribe(( noti ) => {
      console.log(noti.notification.payload.additionalData);
      this.storage.removeDeudaRef();
      switch (noti.notification.payload.additionalData.modulo) {
        case 'sCD':
          let deuda = noti.notification.payload.additionalData.deudaData;
          console.log(deuda)
          deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          this.storage.setDoubtDetail( JSON.stringify( deuda ) );
          this.navCtrl.navigateForward( '/tabs/pendientes/detalle-deuda' );
        break;
        case 'rC':
          let rechazo = noti.notification.payload.additionalData.deudaData;
          console.log(rechazo)
          this.storage.setDoubtDetail( JSON.stringify( rechazo ) );
          this.navCtrl.navigateForward( '/tabs/pendientes/rechazo-cobro' );
        break;
        case 'pD':
          let pago = noti.notification.payload.additionalData.deudaData;
          console.log(pago)
          pago.monto = Number(pago.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          this.storage.setDoubtDetail( JSON.stringify( pago ) );
          this.navCtrl.navigateForward( '/tabs/pendientes/detalle-pago' );
        break;
        case 'dD':
          let deudaD = noti.notification.payload.additionalData.deudaData;
          console.log(deudaD)
          deudaD.monto = Number(deudaD.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          this.storage.setDoubtDetail( JSON.stringify( deudaD ) );
          this.navCtrl.navigateForward( '/tabs/pendientes/detalle-deuda' );
        break;
        case 'rP':
          let rechazoP = noti.notification.payload.additionalData.deudaData;
          console.log(rechazoP)
          this.storage.setDoubtDetail( JSON.stringify( rechazoP ) );
          this.navCtrl.navigateForward( '/tabs/pendientes/rechazar-pago' );
        break;
        default:
          break;
      }
    });
    console.log('One signal init')

    this.oneSignal.getIds().then( async info => {
      console.log(info);
      let data = {
        fireToken: info.userId
      };
      (await this.businessService.setTokenPush(data))
      .subscribe( (resp : any) => {
        console.log(resp);
        console.log('Token one signal registrado')
      }, (err) => {
        console.log(err);
      });
    }, (err) => {
      console.log(err)
    })
    
    this.oneSignal.endInit();
  }

}
