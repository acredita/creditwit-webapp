import { Injectable } from "@angular/core";
import { NavController } from "@ionic/angular";
import { SecurityService } from "./security.service";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  constructor(
    private navCtrl: NavController,
    private securityService: SecurityService
  ) {}

  clearStorage() {
    localStorage.clear();
  }

  // Contexto de la app, usado para obtener y setear informacion util para navegacion
  setContext(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("context"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getContext() {
    let context = localStorage.getItem(
      this.securityService.encryptUsingAES256("context")
    );
    if (context) {
      return this.securityService.decryptUsingAES256(context);
    } else {
      return null;
    }
  }

  setRegistro1(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("registro1"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getRegistro1() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("registro1")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setRegistro2(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("registro2"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getRegistro2() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("registro2")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setRefreshToken(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("refreshToken"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getRefreshToken() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("refreshToken")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setUserData(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("userData"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getUserData() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("userData")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setCedula(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("cedula"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getCedula() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("cedula")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setTelefono(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("telefono"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getTelefono() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("telefono")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setToken(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("token"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getToken() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("token")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setCuentas(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("cuentas"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getCuentas() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("cuentas")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return "[]";
    }
  }

  setCuenta(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("cuenta"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getCuenta() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("cuenta")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  removeCuenta() {
    localStorage.removeItem(this.securityService.encryptUsingAES256("cuenta"));
  }

  setCuentaExito(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("cuentaExito"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getCuentaExito() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("cuentaExito")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setRatingHistory(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("ratingHistory"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getRatingHistory() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("ratingHistory")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }
  //COBROS DATA

  setDeuda(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("deudorData"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getDeuda() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("deudorData")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setRecordatorio(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("recordatorio"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getRecordatorio() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("recordatorio")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  //PENDIENTES DATA

  getDoubtDetail() {
    // obtiene los detalles de la deuda que el user seleccione para lectura
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("doubtDetail")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setDoubtDetail(data) {
    // set los detalles de la deuda que el user selecciona
    localStorage.setItem(
      this.securityService.encryptUsingAES256("doubtDetail"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getPago() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("pago")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  setPago(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("pago"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  async validarToken(): Promise<boolean> {
    if (this.getToken() == null || this.getToken() == "null") {
      this.navCtrl.navigateRoot("/login");
      return Promise.resolve(false);
    } else {
      return Promise.resolve(true);
    }
  }

  // Info proveniente de SMS
  setDeudaRef(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("deudaRef"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getDeudaRef() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("deudaRef")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  removeDeudaRef() {
    localStorage.removeItem(
      this.securityService.encryptUsingAES256("deudaRef")
    );
  }

  setPath(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("path"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getPath() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("path")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  removePath() {
    localStorage.removeItem(this.securityService.encryptUsingAES256("path"));
  }

  setDirectorio(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("directorio"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getDirectorio() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("directorio")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  removeDirectorio() {
    localStorage.removeItem(this.securityService.encryptUsingAES256("directorio"));
  }

  setUserDirectorio(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("userDirectorio"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getUserDirectorio() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("userDirectorio")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  removeUserDirectorio() {
    localStorage.removeItem(this.securityService.encryptUsingAES256("userDirectorio"));
  }

  setListDeudas(data) {
    localStorage.setItem(
      this.securityService.encryptUsingAES256("listDeudas"),
      this.securityService.encryptUsingAES256(data)
    );
  }

  getListDeudas() {
    let data = localStorage.getItem(
      this.securityService.encryptUsingAES256("listDeudas")
    );
    if (data) {
      return this.securityService.decryptUsingAES256(data);
    } else {
      return null;
    }
  }

  removeListDeudas() {
    localStorage.removeItem(this.securityService.encryptUsingAES256("listDeudas"));
  }
}
