import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";
import { NavController } from '@ionic/angular';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // url: string = "https://testapi.creditwit.app/access"; //Desarrollo
  url: string = "https://api.creditwit.app/access"; //Produccion

  constructor(private http: HttpClient,
    private storage: StorageService,
    private navCtrl: NavController) { }

  async refreshTokenJWT() {
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(this.storage.getToken());
    if (isExpired) {
      try {
        let token: any = await this.refreshToken().toPromise();
        console.log(token)
        this.storage.setToken(token.accessToken);
      }
      catch (err) {
        localStorage.clear();
        Swal.fire({
          confirmButtonColor: '#E7A016',
          text: 'Sesión expiró por seguridad',
          heightAuto: false,
          showCloseButton: true,
          confirmButtonText: 'Aceptar',
        });
        this.navCtrl.navigateRoot('');
      }
    }
  }

  login(data) {
    return this.http.post(this.url + '/login', data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  refreshToken() {
    return this.http.get(this.url + '/newAccess', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getRefreshToken(),
      },
    });
  }

  passReset(data) {
    return this.http.post(this.url + '/resetPass', data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  checkCode(data) {
    return this.http.get(this.url + '/checkCode/' + data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getRefreshToken()
      },
    });
  }

  async changePass(data) {
    await this.refreshTokenJWT();
    return this.http.patch(this.url + '/resetPass', data, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this.storage.getToken()
      },
    });
  }

}
