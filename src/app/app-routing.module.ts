
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UserGuard } from './guards/user.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then( m => m.TabsPageModule),
    canLoad: [ UserGuard ]
  },
  {
    path: 'registro1',
    loadChildren: () => import('./pages/registro/registro1/registro1.module').then( m => m.Registro1PageModule)
  },
  {
    path: 'registro2',
    loadChildren: () => import('./pages/registro/registro2/registro2.module').then( m => m.Registro2PageModule)
  },
  {
    path: 'registro3',
    loadChildren: () => import('./pages/registro/registro3/registro3.module').then( m => m.Registro3PageModule)
  },
  {
    path: 'contrasena',
    loadChildren: () => import('./pages/olvido-contrasena/contrasena/contrasena.module').then( m => m.ContrasenaPageModule)
  },
  {
    path: 'confirmar-telefono',
    loadChildren: () => import('./pages/olvido-contrasena/confirmar-telefono/confirmar-telefono.module').then( m => m.ConfirmarTelefonoPageModule)
  },
  {
    path: 'cambiar-clave',
    loadChildren: () => import('./pages/olvido-contrasena/cambiar-clave/cambiar-clave.module').then( m => m.CambiarClavePageModule)
  },
  {
    path: 'detalle-pago',
    loadChildren: () => import('./pages/pendientes/acreedor/detalle-pago/detalle-pago.module').then( m => m.DetallePagoPageModule)
  },
  {
    path: 'rechazo-cobro',
    loadChildren: () => import('./pages/pendientes/acreedor/rechazo-cobro/rechazo-cobro.module').then( m => m.RechazoCobroPageModule)
  },
  {
    path: 'califica-deudor',
    loadChildren: () => import('./pages/pendientes/acreedor/califica-deudor/califica-deudor.module').then( m => m.CalificaDeudorPageModule)
  },
  {
    path: 'registro-pago',
    loadChildren: () => import('./pages/pendientes/deudor/registro-pago/registro-pago.module').then( m => m.RegistroPagoPageModule)
  },
  {
    path: 'confirmar-pago',
    loadChildren: () => import('./pages/pendientes/deudor/confirmar-pago/confirmar-pago.module').then( m => m.ConfirmarPagoPageModule)
  },
  {
    path: 'rechazar-pago',
    loadChildren: () => import('./pages/pendientes/deudor/rechazar-pago/rechazar-pago.module').then( m => m.RechazarPagoPageModule)
  },
  {
    path: 'sCD/:id',
    // detalle deuda publica
    loadChildren: () => import('./pages/public/detalle-deuda-pub/detalle-deuda-pub.module').then( m => m.DetalleDeudaPubPageModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'login?d={idDeuda}&m={modulo}',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
];



@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
