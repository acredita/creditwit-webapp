import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { NavController, LoadingController } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";
import { StorageService } from "src/app/services/storage.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-contrasena",
  templateUrl: "./contrasena.page.html",
  styleUrls: ["./contrasena.page.scss"]
})
export class ContrasenaPage implements OnInit {
  contrasenaForm: FormGroup;
  idInvalid: boolean = false;
  loading: any;
  fixedID: string;

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private authService: AuthService,
    private storage: StorageService,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.contrasenaForm = this.fb.group({
      letter: ["V", [Validators.required]],
      id: ["", [Validators.required]]
    });
  }

  onInput(event: any) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  get errorControl() {
    return this.contrasenaForm.controls;
  }

  async onContinue() {
    const form = this.contrasenaForm.value;
    this.idInvalid = false;
    let data = {
      user:
        form.letter +
        form.id
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
    };
    if (form.id != "") {
      this.presentLoading("Cargando...");
      this.authService.passReset(data).subscribe(
        (resp: any) => {
          console.log(resp);
          this.storage.setRefreshToken(resp.refreshToken);
          this.storage.setTelefono(resp.telefono);
          this.storage.setCedula(
            form.letter +
              Number(
                form.id
                  .replace(",", "")
                  .replace(",", "")
                  .replace(",", "")
                  .replace(",", "")
                  .replace(".", "")
                  .replace(".", "")
                  .replace(".", "")
                  .replace(".", "")
              ).toLocaleString("de-DE")
          );
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
          this.navCtrl.navigateForward("/confirmar-telefono");
        },
        (err) => {
          console.log(err);
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
          if (err.error.reason) {
            Swal.fire({
              imageUrl: "./assets/images/icons/warning.svg",
              imageHeight: 80,
              imageWidth: 80,
              imageAlt: "A tall image",
              confirmButtonColor: "#E7A016",
              text: err.error.reason,
              heightAuto: false,
              showCloseButton: true
            });
          }
        }
      );
    } else {
      this.idInvalid = true;
    }
  }

  goBack() {
    this.navCtrl.navigateBack("/login");
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  formatID(event: any) {
    //console.log(' ID ' + event.target.value);
    if (event.detail.value !== "") {
      let value = String(event.detail.value)
        .replace(".", "")
        .replace(".", "")
        .replace(".", "")
        .replace(",", "")
        .replace(",", "")
        .replace(",", "");
      this.fixedID = parseInt(value)
        .toString()
        .replace(/\./g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    //console.log('fixed ID ' + this.fixedID);
  }
}
