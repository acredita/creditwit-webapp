import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cambiar-clave',
  templateUrl: './cambiar-clave.page.html',
  styleUrls: ['./cambiar-clave.page.scss'],
})
export class CambiarClavePage implements OnInit {

  changePassForm: FormGroup;
  passInvalid: boolean = false;
  confirmInvalid: boolean = false;
  hide: boolean = false;
  error: boolean = false;
  loading: any;
  hideC: boolean;

  constructor( public fb: FormBuilder,
    private navCtrl: NavController,
    private authService: AuthService,
    private storage: StorageService,
    private loadingCtrl: LoadingController ) { }

  ngOnInit() {
    this.changePassForm = this.fb.group({
      pass: ['', [Validators.required, Validators.minLength(8)]],
      confirm: ['', [Validators.required]]
    });
  }

  get errorControl() {
    return this.changePassForm.controls;
  }

  async onContinue() {
    const form = this.changePassForm.value;
    this.passInvalid = false;
    this.confirmInvalid = false;
    this.error = false;
    if (form.pass == ''){
      this.passInvalid = true;
    }
    if (form.confirm == ''){
      this.confirmInvalid = true;
    }

    if ( this.changePassForm.valid && form.pass != form.confirm  ){
      this.error = true;
    }

    if ( this.changePassForm.valid && form.pass == form.confirm){
      let data = {
        contraseña: form.pass
      }
      this.presentLoading( 'Cargando...' );
      (await this.authService.changePass(data))
      .subscribe( (resp : any) => {
        console.log(resp);
        //this.storage.setRefreshToken( resp.refreshToken );
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        this.navCtrl.navigateRoot( '/login' );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        if (err.error.error_description){
          Swal.fire({
            imageUrl: './assets/images/icons/warning.svg',
            imageHeight: 80,
            imageWidth: 80,
            imageAlt: 'A tall image',
            confirmButtonColor: '#E7A016',
            text: err.error.error_description,
            heightAuto: false,
            showCloseButton: true,
          });
        }
      });
    }

  }

  hidden(){
    this.hide = !this.hide;
  }

  hiddenC(){
    this.hideC = !this.hideC;
  }

  goBack(){
    this.navCtrl.navigateBack('/confirmar-telefono');
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

}
