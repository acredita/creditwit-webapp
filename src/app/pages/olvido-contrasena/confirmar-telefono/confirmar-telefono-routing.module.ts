import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmarTelefonoPage } from './confirmar-telefono.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmarTelefonoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmarTelefonoPageRoutingModule {}
