import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-confirmar-telefono',
  templateUrl: './confirmar-telefono.page.html',
  styleUrls: ['./confirmar-telefono.page.scss'],
})
export class ConfirmarTelefonoPage implements OnInit {

  confirmarForm: FormGroup;
  cedula: string;
  telefono: string;
  codInvalid: boolean = false;
  error: boolean = false;
  loading: any;
  keyboard: boolean = true;

  constructor( public fb: FormBuilder,
    private navCtrl: NavController,
    private toastController: ToastController,
    private authService: AuthService,
    private storage: StorageService,
    private loadingCtrl: LoadingController ) { }

  ngOnInit() {
    this.confirmarForm = this.fb.group({
      cod: ['', [Validators.required]]
    });
    this.telefono = this.storage.getTelefono();
    this.cedula = this.storage.getCedula();
    window.addEventListener('keyboardDidHide', () => {
      this.keyboard = true;
    });
    window.addEventListener('keyboardDidShow', (event) => {
      this.keyboard = false;
    });
  }

  async clipboard(){
    let copyText = "info@creditwit.app";
    var textArea = document.getElementById('email') as HTMLInputElement;

    textArea.select();
    textArea.setSelectionRange(0, 99999);
    /* Copy the text inside the text field */
    document.execCommand("copy");
      
    /* Alert the copied text */
    const toast = await this.toastController.create({
      message: 'Correo copiado',
      duration: 2000
    });
    toast.present();
  }

  get errorControl() {
    return this.confirmarForm.controls;
  }

  goBack(){
    this.navCtrl.navigateBack('/contrasena');
  }

  async resetCode(){
    let data = {
      user: this.cedula.replace('.', '').replace('.', '').replace('.', '').replace('.', '')
    }
    this.authService.passReset( data )
      .subscribe( async (resp : any) => {
        const toast = await this.toastController.create({
          message: 'Código reenviado',
          duration: 2000
        });
        toast.present();
      }, (err) => {
        console.log(err);
      });
  }

  async onContinue() {
    const form = this.confirmarForm.value;
    this.error = false;
    if (form.cod == ''){
      this.codInvalid = true;
    }

    if ( this.confirmarForm.valid ){
      let data = form.cod;
      this.presentLoading( 'Cargando...' );
      this.authService.checkCode( data )
      .subscribe( (resp : any) => {
        console.log(resp)
        this.storage.setToken( resp.token );
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        this.navCtrl.navigateForward( '/cambiar-clave' );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        this.error = true;
      });
    }
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

}
