import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmarTelefonoPageRoutingModule } from './confirmar-telefono-routing.module';

import { ConfirmarTelefonoPage } from './confirmar-telefono.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmarTelefonoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ConfirmarTelefonoPage]
})
export class ConfirmarTelefonoPageModule {}
