import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, Platform } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-detalle-deuda',
  templateUrl: './detalle-deuda.page.html',
  styleUrls: ['./detalle-deuda.page.scss'],
})
export class DetalleDeudaPage implements OnInit {

  pago: any;

  constructor(private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
    private platform: Platform,
    private photoViewer: PhotoViewer) { }

  ngOnInit() {
    if (this.platform.is('ios') && this.platform.is('cordova')) {
      document.getElementById('content-principal-deuda-historial').classList.add('content-principal-ios');
    }
    this.pago = JSON.parse(this.storage.getPago());
    console.log(this.pago)
    this.pago.acreedor.telefono = `0${this.pago.acreedor.telefono.substring(3, 6)} ${this.pago.acreedor.telefono.substring(6, 9)} ${this.pago.acreedor.telefono.substring(9, this.pago.acreedor.telefono.length)}`;
    if (!this.pago.concepto) {
      this.pago.concepto = 'Sin concepto'
    }
    if (!this.pago.referencia) {
      this.pago.referencia = 'N/A';
    }

    if (this.pago.abonada?.montoDeuda) {
      this.pago.abonada.montoDeuda = Number(this.pago.abonada.montoDeuda / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      this.pago.abonada.abonado = Number(this.pago.abonada.abonado / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      if (this.pago.abonada.concepto.length > 0) {
        if (this.pago.abonada.concepto == '') {
          this.pago.abonada.concepto = 'Sin concepto';
        }
      } else {
        this.pago.abonada.concepto = 'Sin concepto';
      }
    }

    this.pago.pagas?.forEach(pago => {
      pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      if (!pago.concepto) {
        pago.concepto = 'Sin concepto'
      }
    });

  }

  goBack() {
    this.navCtrl.navigateBack('/tabs/historial');
  }

  verImagen() {
    // if ( this.platform.is('cordova') ){
    //   this.photoViewer.show('https://' + this.pago.comprobante);
    // }
    // else{
    // }
    Swal.fire({
      html: `<a href="https://${this.pago.comprobante}" target="_blank"><img src="https://${this.pago.comprobante}" target="_blank" alt=""></a>`,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Cerrar',
      heightAuto: false,
      showCloseButton: true,
    });
  }

}
