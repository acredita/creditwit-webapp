import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll, LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { ExportExcelService } from 'src/app/services/export-excel.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.page.html',
  styleUrls: ['./historial.page.scss'],
})
export class HistorialPage implements OnInit {

  @ViewChild(IonInfiniteScroll, { static: false }) infitniteScroll: IonInfiniteScroll;
  deudor: boolean;
  loading: any;
  cobros = [];
  pagos = [];
  page = 0;
  fechaInicio = new Date();
  fechaFin = new Date();

  dataForExcel = [];

  constructor(private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
    private exportExcelService: ExportExcelService) { }

  ngOnInit() {
    let userData = JSON.parse(this.storage.getUserData());
    if (userData.tipo == 'Acreedor') {
      this.deudor = false;
    }
    else {
      this.deudor = true;
    }
  }

  ionViewWillEnter() {
    this.cobros = [];
    this.pagos = [];
    this.page = 0;
    this.getHistorial(this.page, 10);
  }

  cobrar() {
    this.deudor = false;
  }

  pagar() {
    this.deudor = true;
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  async getHistorial(salta, limite) {
    let data = {
      salta: salta,
      limite: limite
    };
    (await this.businessService.getHistorial(data))
      .subscribe((resp: any) => {
        let cobros = [];
        let pagos = [];
        console.log(resp)
        cobros = resp.cobros;
        pagos = resp.pagos;
        pagos.forEach(pago => {
          if (pago.currency == 'usd') {
            pago.currency = '$';
          }
          pago.acreedor.cedula = `${pago.acreedor.cedula.substring(0, 1)} ${Number(pago.acreedor.cedula.substring(1, pago.acreedor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.pagos.push(pago);
        });
        cobros.forEach(cobro => {
          if (cobro.currency == 'usd') {
            cobro.currency = '$';
          }
          cobro.deudor.cedula = `${cobro.deudor.cedula.substring(0, 1)} ${Number(cobro.deudor.cedula.substring(1, cobro.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          cobro.monto = Number(cobro.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.cobros.push(cobro);
        });
      }, (err) => {
        console.log(err);
      });
  }

  async loadHistorial(salta, limite, event) {
    let data = {
      salta: salta,
      limite: limite
    };
    (await this.businessService.getHistorial(data))
      .subscribe((resp: any) => {
        let cobros = [];
        let pagos = [];
        console.log(resp)
        cobros = resp.cobros;
        pagos = resp.pagos;
        if (cobros.length < 10 && pagos.length < 10) {
          event.target.complete();
          this.infitniteScroll.disabled = true;
          return;
        }
        pagos.forEach(pago => {
          if (pago.currency == 'usd') {
            pago.currency = '$';
          }
          pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.pagos.push(pago);
        });
        cobros.forEach(cobro => {
          if (cobro.currency == 'usd') {
            cobro.currency = '$';
          }
          cobro.deudor.cedula = `${cobro.deudor.cedula.substring(0, 1)} ${Number(cobro.deudor.cedula.substring(1, cobro.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          cobro.monto = Number(cobro.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.cobros.push(cobro);
        });
      }, (err) => {
        console.log(err);
      });
  }

  async loadData(event) {
    let salta = this.page + 10;
    let limite = 10;
    this.page = salta;

    let data = {
      salta: salta,
      limite: limite
    };
    (await this.businessService.getHistorial(data))
      .subscribe((resp: any) => {
        let cobros = [];
        let pagos = [];
        console.log(resp)
        cobros = resp.cobros;
        pagos = resp.pagos;
        if (cobros.length < 10 && pagos.length < 10) {
          this.infitniteScroll.disabled = true;
        }
        pagos.forEach(pago => {
          if (pago.currency == 'usd') {
            pago.currency = '$';
          }
          pago.acreedor.cedula = `${pago.acreedor.cedula.substring(0, 1)} ${Number(pago.acreedor.cedula.substring(1, pago.acreedor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.pagos.push(pago);
        });
        cobros.forEach(cobro => {
          if (cobro.currency == 'usd') {
            cobro.currency = '$';
          }
          cobro.deudor.cedula = `${cobro.deudor.cedula.substring(0, 1)} ${Number(cobro.deudor.cedula.substring(1, cobro.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          cobro.monto = Number(cobro.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.cobros.push(cobro);
        });
        event.target.complete();
      }, (err) => {
        console.log(err);
      });

  }

  goToPago(pago) {
    this.storage.setPago(JSON.stringify(pago));
    this.navCtrl.navigateForward('/tabs/historial/detalle-deuda');
  }

  goToCobro(cobro) {
    this.storage.setPago(JSON.stringify(cobro));
    this.navCtrl.navigateForward('/tabs/historial/detalle-cobro');
  }

  async exportToExcel() {

    let data = {
      desde: `${this.fechaInicio.getFullYear()}-${this.fechaInicio.getMonth() + 1}-${this.fechaInicio.getDate()}`,
      hasta: `${this.fechaFin.getFullYear()}-${this.fechaFin.getMonth() + 1}-${this.fechaFin.getDate()}`
    };
    console.log(data);
    (await this.businessService.getHistorialFiltro(data))
      .subscribe((resp: any) => {
        console.log(resp)
        let dataExcel = [];
        resp[0].pagos.forEach(pago => {
          if (pago.currency == 'usd') {
            pago.currency = '$';
          }
          pago.acreedor.cedula = `${pago.acreedor.cedula.substring(0, 1)} ${Number(pago.acreedor.cedula.substring(1, pago.acreedor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          let row = {
            FECHA: pago.fechaConciliacion,
            NOMBRE: `${pago.acreedor.nombres} ${pago.acreedor.apellidos}`,
            CEDULA: pago.acreedor.cedula,
            MONTO: `${pago.currency} ${pago.monto}`,
            ESTATUS: pago.estado == 'confirmado' ? 'Pago recibido' : 'Deuda rechazada'
          }
          dataExcel.push(row);
        });
        resp[0].cobros.forEach(cobro => {
          if (cobro.currency == 'usd') {
            cobro.currency = '$';
          }
          cobro.deudor.cedula = `${cobro.deudor.cedula.substring(0, 1)} ${Number(cobro.deudor.cedula.substring(1, cobro.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`
          cobro.monto = Number(cobro.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          let row = {
            FECHA: cobro.fechaConciliacion,
            NOMBRE: `${cobro.deudor.nombres} ${cobro.deudor.apellidos}`,
            CEDULA: cobro.deudor.cedula,
            MONTO: `${cobro.currency} ${cobro.monto}`,
            ESTATUS: cobro.estado == 'confirmado' ? 'Pago recibido' : 'Deuda incobrable'
          }
          dataExcel.push(row);
        });
        dataExcel.forEach((row: any) => {
          this.dataForExcel.push(Object.values(row))
        })

        this.exportExcelService.exportAsExcelFile(dataExcel, 'Historial Creditwit');

      }, (err) => {
        console.log(err);
      });

  }

  parseDate(dateString: string): Date {
    if (dateString) {
      return new Date(dateString);
    }
    return null;
  }

}
