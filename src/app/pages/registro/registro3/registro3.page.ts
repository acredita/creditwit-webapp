import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import {
  NavController,
  ToastController,
  LoadingController,
  Platform
} from "@ionic/angular";
import { ClientesService } from "src/app/services/clientes.service";
import { StorageService } from "src/app/services/storage.service";
import Swal from "sweetalert2";
import { PushService } from "../../../services/push.service";
import { BusinessService } from "../../../services/business.service";

@Component({
  selector: "app-registro3",
  templateUrl: "./registro3.page.html",
  styleUrls: ["./registro3.page.scss"]
})
export class Registro3Page implements OnInit {
  registro3Form: FormGroup;
  registro2: any;
  telefono: string;
  codInvalid: boolean = false;
  loading: any;
  keyboard: boolean = true;

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private toastController: ToastController,
    private clientesService: ClientesService,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private pushService: PushService,
    private businessService: BusinessService
  ) {}

  ngOnInit() {
    this.registro3Form = this.fb.group({
      cod: ["", [Validators.required]]
    });
    this.registro2 = JSON.parse(this.storage.getRegistro2());
    this.telefono = this.registro2.phone;
    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  async onContinue() {
    const form = this.registro3Form.value;

    if (form.cod == "") {
      this.codInvalid = true;
    }

    if (this.registro3Form.valid) {
      let data = {
        codigo: form.cod
      };
      this.presentLoading("Cargando...");
      this.clientesService.confirmClient(data).subscribe(
        (resp: any) => {
          this.storage.setRefreshToken(resp.refreshToken);
          this.storage.setToken(resp.accessToken);
          this.storage.setUserData(JSON.stringify(resp.userData));
          this.storage.setCuentas(JSON.stringify(resp.userData.payMethods));
          //Falta algo
          if (
            this.platform.is("cordova") &&
            resp.userData.config.pushNotifications
          ) {
            if (resp.userData.config.playerId) {
              this.pushService.configuracionInicial();
              this.pushService.configuracionInicial();
            } else {
              Swal.fire({
                text: "¿Desea recibir notificaciones?",
                showCancelButton: true,
                confirmButtonColor: "#E7A016",
                confirmButtonText: "Si",
                cancelButtonColor: "#E7A016",
                cancelButtonText: "No",
                heightAuto: false
              }).then(async (result) => {
                if (result.value) {
                  this.pushService.configuracionInicial();
                  this.pushService.configuracionInicial();
                } else {
                  (await this.businessService.activePush()).subscribe(
                    (resp: any) => {
                      console.log(resp);
                      let userData = JSON.parse(this.storage.getUserData());
                      userData.config.pushNotifications =
                        resp.pushNotifications;
                      this.storage.setUserData(JSON.stringify(userData));
                    },
                    (err) => {
                      console.log(err);
                    }
                  );
                }
              });
            }
          }
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
          this.navCtrl.navigateForward("/tabs/enviar-cobro");
        },
        (err) => {
          console.log(err);
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
          if (err.error.error == "unAuthorized") {
            Swal.fire({
              confirmButtonColor: "#E7A016",
              text: err.error.error_description,
              heightAuto: false,
              showCloseButton: true
            });
          } else {
            Swal.fire({
              confirmButtonColor: "#E7A016",
              text: "Código inválido, intenta de nuevo.",
              heightAuto: false,
              showCloseButton: true
            });
          }
        }
      );
    }
  }

  resetCode() {
    this.clientesService.resetConfirmClient().subscribe(
      (resp: any) => {
        Swal.fire({
          confirmButtonColor: "#E7A016",
          text: "Código de confirmación reenviado.",
          heightAuto: false,
          showCloseButton: true
        });
      },
      (err) => {
        console.log(err);
        if (err.error.error == "unAuthorized") {
          Swal.fire({
            confirmButtonColor: "#E7A016",
            text: err.error.error_description,
            heightAuto: false,
            showCloseButton: true
          });
        }
      }
    );
  }

  async clipboard() {
    let copyText = "info@creditwit.app";
    var textArea = document.getElementById("email") as HTMLInputElement;

    textArea.select();
    textArea.setSelectionRange(0, 99999);
    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    const toast = await this.toastController.create({
      message: "Correo copiado",
      duration: 2000
    });
    toast.present();
  }

  get errorControl() {
    return this.registro3Form.controls;
  }

  goBack() {
    this.navCtrl.navigateBack("/registro2");
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }
}
