import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-registro4',
  templateUrl: './registro4.page.html',
  styleUrls: ['./registro4.page.scss'],
})
export class Registro4Page implements OnInit {
  loading: HTMLIonLoadingElement;

  constructor( private navCtrl: NavController,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService ) { }

  ngOnInit() {
  }

  async setDeudor(){
    let data = {
      tipo: 'Deudor'
    }
    this.presentLoading( 'Cargando...' );
    (await this.businessService.typeClient(data))
      .subscribe( (resp : any) => {
        console.log(resp)
        this.storage.setUserData( resp );
        this.navCtrl.navigateRoot('/tabs')
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      });
  }

  async setPagador(){
    let data = {
      tipo: 'Pagador'
    }
    this.presentLoading( 'Cargando...' );
    (await this.businessService.typeClient(data))
      .subscribe( (resp : any) => {
        console.log(resp)
        this.storage.setUserData( resp );
        this.navCtrl.navigateRoot('/tabs')
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      });
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

}
