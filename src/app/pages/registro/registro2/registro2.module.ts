import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Registro2PageRoutingModule } from './registro2-routing.module';

import { Registro2Page } from './registro2.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Registro2PageRoutingModule,
    ReactiveFormsModule,
    BrMaskerModule
  ],
  declarations: [Registro2Page]
})
export class Registro2PageModule {}
