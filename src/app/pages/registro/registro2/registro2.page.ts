import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { ClientesService } from 'src/app/services/clientes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro2',
  templateUrl: './registro2.page.html',
  styleUrls: ['./registro2.page.scss'],
})
export class Registro2Page implements OnInit {

  registro2Form: FormGroup;
  hide: boolean = false;
  registro1: any;
  tittle: any;
  nameInvalid: boolean = false;
  lastNameInvalid: boolean = false;
  emailInvalid: boolean = false;
  passInvalid: boolean = false;
  phoneInvalid: boolean = false;
  phone: string = '';
  registrado: boolean = false;
  cedula: string = '';
  loading: any;

  constructor(public fb: FormBuilder,
    private navCtrl: NavController,
    private storage: StorageService,
    private clientesService: ClientesService,
    private loadingCtrl: LoadingController, ) { }

  ngOnInit() {

    this.registro2Form = this.fb.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      code: ['0414'],
      phone: ['', [Validators.required]],
      pass: ['', [Validators.required, Validators.minLength(8)]]
    });

    this.registro1 = JSON.parse(this.storage.getRegistro1());

    if ( this.registro1.registro ){
      // this.tittle = this.registro1.nombres + ' ' + this.registro1.apellidos;
      this.tittle = `${this.registro1.letter}${Number(this.registro1.id).toLocaleString('de-DE')}`;
      this.cedula = `${this.registro1.letter}${Number(this.registro1.id).toLocaleString('de-DE')}`;
      this.registrado = true;
      this.registro2Form.patchValue({
        name: this.registro1.nombres,
        lastName: this.registro1.apellidos,
        phone: '0' + this.registro1.telefono.replace('+58','')
      })
    }
    else{
      this.registrado = false;
      this.tittle = `${this.registro1.letter}${Number(this.registro1.id).toLocaleString('de-DE')}`;
    }
  }

  get errorControl() {
    return this.registro2Form.controls;
  }

  async onContinue() {
    const form = this.registro2Form.value;
    this.nameInvalid = false;
    this.lastNameInvalid = false;
    this.emailInvalid = false;
    this.passInvalid = false;
    this.phoneInvalid = false;
    if (form.name == ''){
      this.nameInvalid = true;
    }
    if (form.lastName == ''){
      this.lastNameInvalid = true;
    }
    if (form.email == ''){
      this.emailInvalid = true;
    }
    if (form.phone == ''){
      this.phoneInvalid = true;
    }
    if (form.pass == ''){
      this.passInvalid = true;
    }

    if ( this.registro2Form.valid ){
      let data = {
        correo: form.email,
        cedula: `${this.registro1.letter}${this.registro1.id}`,
        nombres: form.name,
        telefono: `+58${form.code}${form.phone}` ,
        contraseña: form.pass,
        apellidos: form.lastName
      }
      this.presentLoading( 'Cargando...' );
      this.clientesService.registro( data )
      .subscribe( (resp : any) => {
        console.log(resp.refreshToken);
        this.storage.setRefreshToken( resp.refreshToken );
        this.storage.setRegistro2( JSON.stringify(form) );
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        this.navCtrl.navigateForward('/registro3');
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        if ( err.error.error == 'Datos usados' ){
          Swal.fire({
            imageUrl: './assets/images/icons/warning.svg',
            imageHeight: 80,
            imageWidth: 80,
            imageAlt: 'A tall image',
            confirmButtonColor: '#E7A016',
            text: err.error.error_description,
            heightAuto: false,
            showCloseButton: true,
          });
        }
      });
    }
  }

  hidden(){
    this.hide = !this.hide;
  }

  showTerminos(){
    Swal.fire({
      html:
      '<div style=\"height: 80vh; overflow: auto;\">' +
        '<p style=\"text-align: left; margin-top: 10px;\"><b>Términos y condiciones</b></p>' +
        '<br>' +
        '<p style=\"text-align: justify;\">Arrived compass prepare an on as. Reasonable particular on my it in sympathize. Size now easy eat hand how. Unwilling he departure elsewhere dejection at. Heart large seems may purse means few blind. Exquisite newspaper attending on certainty oh suspicion of. He less do quit evil is. Add matter family active mutual put wishes happen. </p>' +
        '<p style=\"text-align: justify;\">Do greatest at in learning steepest. Breakfast extremity suffering one who all otherwise suspected. He at no nothing forbade up moments. Wholly uneasy at missed be of pretty whence. John way sir high than law who week. Surrounded prosperous introduced it if is up dispatched. Improved so strictly produced answered elegance is. </p>' +
        '<p style=\"text-align: justify;\">When be draw drew ye. Defective in do recommend suffering. House it seven in spoil tiled court. Sister others marked fat missed did out use. Alteration possession dispatched collecting instrument travelling he or on. Snug give made at spot or late that mr.</p>' +
        '<p style=\"text-align: justify;\">May indulgence difficulty ham can put especially. Bringing remember for supplied her why was confined. Middleton principle did she procuring extensive believing add. Weather adapted prepare oh is calling. These wrong of he which there smile to my front. He fruit oh enjoy it of whose table. </p>' +
        '</div>',
      showCloseButton: true,
      showCancelButton: false,
      heightAuto: false,
      showConfirmButton: false
    })
  }

  goBack(){
    this.navCtrl.navigateBack('/registro1');
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  onInput(event:any){
    let regex = /[0-9]/;  

    if (!regex.test(event.key)) {
      if (event.key !== 'Backspace'){
        event.preventDefault();
      }
    }
  }

}
