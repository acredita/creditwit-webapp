import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { NavController, LoadingController } from "@ionic/angular";
import { StorageService } from "src/app/services/storage.service";
import { ClientesService } from "src/app/services/clientes.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-registro1",
  templateUrl: "./registro1.page.html",
  styleUrls: ["./registro1.page.scss"]
})
export class Registro1Page implements OnInit {
  registro1Form: FormGroup;
  idInvalid: boolean = false;
  loading: any;
  fixedID: string;

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private storage: StorageService,
    private clientesService: ClientesService,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.registro1Form = this.fb.group({
      letter: ["V", [Validators.required]],
      id: ["", [Validators.required]]
    });
  }

  onInput(event: any) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  get errorControl() {
    return this.registro1Form.controls;
  }

  async onContinue() {
    const form = this.registro1Form.value;
    this.idInvalid = false;
    if (form.id != "") {
      let data = {
        nombres: "",
        apellidos: "",
        letter: form.letter,
        id: form.id
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", ""),
        registro: false,
        telefono: ""
      };
      this.presentLoading("Cargando...");
      this.clientesService
        .getCedula(
          form.letter +
            form.id
              .replace(",", "")
              .replace(",", "")
              .replace(",", "")
              .replace(",", "")
              .replace(".", "")
              .replace(".", "")
              .replace(".", "")
        )
        .subscribe(
          (resp: any) => {
            console.log(resp);
            if (!resp.registrado) {
              data.nombres = resp.nombres;
              data.apellidos = resp.apellidos;
              data.registro = true;
              data.telefono = resp.telefono;
              this.storage.setRegistro1(JSON.stringify(data));
              setTimeout(() => {
                this.loading.dismiss();
              }, 500);
              this.navCtrl.navigateForward("/registro2");
            } else {
              setTimeout(() => {
                this.loading.dismiss();
              }, 500);
              Swal.fire({
                confirmButtonColor: "#E7A016",
                text:
                  "Esta cédula ya está registrada. Verifícala o ingresa por Olvido de Clave para recuperar al usuario.",
                heightAuto: false,
                showCloseButton: true,
                confirmButtonText: "Aceptar"
              }).then((value) => {
                this.navCtrl.navigateRoot("/login");
              });
            }
          },
          (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
            if (err.error == "noExiste") {
              this.storage.setRegistro1(JSON.stringify(data));
              this.navCtrl.navigateForward("/registro2");
            } else if (err.error.error == "cedulaMalformada") {
              Swal.fire({
                confirmButtonColor: "#E7A016",
                text: err.error.error_description,
                heightAuto: false,
                showCloseButton: true
              });
            } else {
              this.storage.setRegistro1(JSON.stringify(data));
              this.navCtrl.navigateForward("/registro2");
            }
          }
        );
    } else {
      this.idInvalid = true;
    }
  }

  goBack() {
    this.navCtrl.navigateBack("/login");
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  formatID(event: any) {
    //console.log(' ID ' + event.target.value);
    if (event.detail.value !== "") {
      let value = String(event.detail.value)
        .replace(".", "")
        .replace(".", "")
        .replace(".", "")
        .replace(",", "")
        .replace(",", "")
        .replace(",", "");
      this.fixedID = parseInt(value)
        .toString()
        .replace(/\./g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    //console.log('fixed ID ' + this.fixedID);
  }
}
