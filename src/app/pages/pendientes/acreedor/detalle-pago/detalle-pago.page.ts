import Swal from 'sweetalert2';
import { stringify } from 'querystring';
import { BusinessService } from 'src/app/services/business.service';
import { NavController, LoadingController, Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


@Component({
  selector: 'app-detalle-pago',
  templateUrl: './detalle-pago.page.html',
  styleUrls: ['./detalle-pago.page.scss'],
})
export class DetallePagoPage implements OnInit {

  deuda: any;
  loading: any;
  incobrable: boolean = false;
  showRating: boolean = false;
  idDeuda: any;
  abonos = [];
  info: any;

  constructor(
    private navCtrl: NavController,
    private localStorage: StorageService,
    private businessService: BusinessService,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private photoViewer: PhotoViewer
  ) { }

  async ngOnInit() {
    // Obtengo la data que se va a mostrar.
    if (this.platform.is('ios') && this.platform.is('cordova')) {
      document.getElementById('content-principal-detalle-pago').classList.add('content-principal-ios');
      document.getElementById('content-detalle-pago').classList.add('content-ios');
    }
    this.idDeuda = JSON.parse(this.localStorage.getDeudaRef());
    if (this.idDeuda) {
      (await this.businessService.getDoubt(this.idDeuda.idDeuda))
        .subscribe((resp: any) => {
          console.log(resp);
          this.deuda = resp;
          if (this.deuda.currency == 'usd') {
            this.deuda.currency = '$';
          }
          this.deuda.monto = (parseFloat(this.deuda.monto) / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0, 1)} ${Number(this.deuda.deudor.cedula.substring(1, this.deuda.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`;
          this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3, 6)} ${this.deuda.deudor.telefono.substring(6, 9)} ${this.deuda.deudor.telefono.substring(9, this.deuda.deudor.telefono.length)}`;
          if (this.deuda.calificacion !== "N/A" && this.deuda.calificacion !== '') {
            this.showRating = true;
          }
          if (!this.deuda.concepto) {
            this.deuda.concepto = 'Sin concepto'
          }
          if (this.deuda.abonada.montoDeuda) {
            this.deuda.abonada.montoDeuda = Number(this.deuda.abonada.montoDeuda / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
            this.deuda.abonada.abonado = Number(this.deuda.abonada.abonado / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
            if (this.deuda.abonada.concepto.length > 0) {
              if (this.deuda.abonada.concepto == '') {
                this.deuda.abonada.concepto = 'Sin concepto';
              }
            } else {
              this.deuda.abonada.concepto = 'Sin concepto';
            }
          }
          this.localStorage.setDoubtDetail(JSON.stringify(resp));
        }, (err) => {
          console.log(err);
        });
    } else {
      this.info = JSON.parse(this.localStorage.getDoubtDetail()); // Obtengo el detalle de la deuda desde local Storage
      if (this.info.deuda) {
        this.deuda = this.info.deuda;
        this.deuda.monto = this.deuda.montoTotal;
        this.abonos = this.deuda.detalleAbonos;
        this.abonos.forEach(abono => {
          if (this.deuda.currency == 'Bs.') {
            abono.abonada.abonado = Number(abono.abonada.abonado / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
          } else {
            abono.abonada.abonado = Number(abono.abonada.abonado / 100).toLocaleString('en', { minimumFractionDigits: 2 });
          }
        });
      } else {
        this.deuda = this.info;
      }
      console.log(this.deuda);
      if (this.deuda.currency == 'usd') {
        this.deuda.currency = '$';
      }
      this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0, 1)} ${Number(this.deuda.deudor.cedula.substring(1, this.deuda.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`;
      this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3, 6)} ${this.deuda.deudor.telefono.substring(6, 9)} ${this.deuda.deudor.telefono.substring(9, this.deuda.deudor.telefono.length)}`;
      if (this.deuda.calificacion !== "N/A" && this.deuda.calificacion !== '') {
        this.showRating = true;
      }
      if (!this.deuda.concepto) {
        this.deuda.concepto = 'Sin concepto'
      }
    }
  }

  ionViewWillEnter() {
    this.incobrable = false;
    this.showRating = false;
    this.deuda = JSON.parse(this.localStorage.getDoubtDetail()); // Obtengo el detalle de la deuda desde local Storage
    console.log(this.deuda);
    if (this.deuda.currency == 'usd') {
      this.deuda.currency = '$';
    }
    this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0, 1)} ${Number(this.deuda.deudor.cedula.substring(1, this.deuda.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`;
    this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3, 6)} ${this.deuda.deudor.telefono.substring(6, 9)} ${this.deuda.deudor.telefono.substring(9, this.deuda.deudor.telefono.length)}`;
    if (this.deuda.calificacion !== "N/A" && this.deuda.calificacion !== '') {
      this.showRating = true;
      console.log('detalle pago ' + JSON.stringify(this.deuda.detallePago));
    }
    if (!this.deuda.referencia) {
      this.deuda.referencia = 'N/A';
    }
    if (!this.deuda.concepto) {
      this.deuda.concepto = 'Sin concepto'
    }

    if (this.deuda.abonada.monto) {
      this.deuda.abonada.monto = Number(this.deuda.abonada.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      this.deuda.abonada.abonado = Number(this.deuda.abonada.abonado / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      if (this.deuda.abonada.concepto.length > 0) {
        if (this.deuda.abonada.concepto == '') {
          this.deuda.abonada.concepto = 'Sin concepto';
        }
      } else {
        this.deuda.abonada.concepto = 'Sin concepto';
      }
    }

    this.deuda.pagas.forEach(pago => {
      pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      if (!pago.concepto) {
        pago.concepto = 'Sin concepto'
      }
    });
  }

  verImagen() {
    // if ( this.platform.is('cordova') ){
    //   this.photoViewer.show('https://' + this.deuda.comprobante);
    // }
    // else{
    // }
    Swal.fire({
      html: `<a href="https://${this.deuda.comprobante}" target="_blank"><img src="https://${this.deuda.comprobante}" target="_blank" alt=""></a>`,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Cerrar',
      heightAuto: false,
      showCloseButton: true,
    });
  }

  goBack() {
    this.navCtrl.navigateBack('/tabs/pendientes');
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }


  async answerPayment(response) {
    // confirma o rechaza el pago
    console.log('Deuda ' + JSON.stringify(response));
    let dataResponse = {
      aceptar: response,
    };

    console.log('Headers ' + this.localStorage.getToken());
    console.log('Respuesta ' + JSON.stringify(dataResponse));
    this.presentLoading('Cargando...');
    console.log("id pago: " + this.deuda._id);
    (await this.businessService.confirmPayment(this.deuda._id, dataResponse))
      .subscribe((resp: any) => {
        console.log('resp: ' + JSON.stringify(resp));
        setTimeout(() => {
          this.loading.dismiss();
          if (response) { // si acepto el pago pasa a la pantalla de calificar al deudor
            this.navCtrl.navigateForward('tabs/califica-deudor');
          } else { // sino vuelve a pendientes para chequear las otras deudas
            this.navCtrl.navigateRoot('tabs/pendientes');
          }
        }, 1000);
      }, (err) => {
        console.log('Error: ' + JSON.stringify(err));
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000);

      });
  }

  rechazarPago() {
    // muestra aviso y llama a endpoint con respuesta negativa
    Swal.fire({
      imageUrl: './assets/images/icons/warning.svg',
      imageHeight: 80,
      imageWidth: 80,
      imageAlt: 'A tall image',
      text: 'Se le informará al pagador que \
    su pago no ha sido procesado.',
      showCancelButton: false,
      showCloseButton: true,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Confirmar',
      heightAuto: false,
    }).then((result) => {
      if (result.value) {
        this.answerPayment(false);
      }
    });
  }

}
