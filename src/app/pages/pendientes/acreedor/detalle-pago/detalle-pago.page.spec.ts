import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetallePagoPage } from './detalle-pago.page';

describe('DetallePagoPage', () => {
  let component: DetallePagoPage;
  let fixture: ComponentFixture<DetallePagoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallePagoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetallePagoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
