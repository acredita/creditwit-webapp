import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallePagoPageRoutingModule } from './detalle-pago-routing.module';

import { DetallePagoPage } from './detalle-pago.page';
import { IonicRatingComponentModule } from 'ionic-rating-component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingComponentModule,
    DetallePagoPageRoutingModule
  ],
  declarations: [DetallePagoPage]
})
export class DetallePagoPageModule {}
