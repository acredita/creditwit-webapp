import { stringify } from 'querystring';
import { BusinessService } from 'src/app/services/business.service';
import { NavController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-cobro',
  templateUrl: './detalle-cobro.page.html',
  styleUrls: ['./detalle-cobro.page.scss'],
})
export class DetalleCobroPage implements OnInit {


  deuda:any;
  loading: any;
  incobrable: boolean = false;
  idDeuda: any;
  info:any;
  abonos = [];


  constructor(
    private navCtrl: NavController,
    private localStorage: StorageService,
    private businessService: BusinessService,
    private loadingCtrl: LoadingController,
  ) { }

  async ngOnInit() {
    // Obtengo la data que se va a mostrar.
    this.idDeuda = JSON.parse(this.localStorage.getDeudaRef());
    if (this.idDeuda) {
      (await this.businessService.getDoubt(this.idDeuda.idDeuda))
        .subscribe((resp: any) => {
          console.log(resp);
          this.deuda = resp;
          if ( this.deuda.currency == 'usd' ){
            this.deuda.currency = '$';
          }
          this.deuda.monto = (parseFloat(this.deuda.monto)/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0, 1)} ${Number(this.deuda.deudor.cedula.substring(1, this.deuda.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`;
          this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3, 6)} ${this.deuda.deudor.telefono.substring(6, 9)} ${this.deuda.deudor.telefono.substring(9, this.deuda.deudor.telefono.length)}`;
          let fechaLimite = new Date();
          fechaLimite.setTime(Date.parse(this.deuda.fechaLimite)); // obtengo la fecha limite de la deuda
          let fecha = new Date(); // obtengo fecha actual
          fecha.setHours(0, 0, 0, 0); // seteo la hora en 0
          let diff = fechaLimite.getTime() - fecha.getTime(); // calculo la diferencia en tiempo entre ambas fechas
          let days = Math.floor(diff / (1000 * 60 * 60 * 24)); // divido la fecha en ms para saber cuantos dias restantes hay
          if (this.deuda.estado !== 'deudaPagada' && days < 0) { // la deuda no ha sido pagada en el plazo por lo tanto se puede marcar como incobrable
            this.incobrable = true;
          }
          if ( !this.deuda.concepto ){
            this.deuda.concepto = 'Sin concepto'
          }
        }, (err) => {
          console.log(err);
        });
    }else{
      this.info = JSON.parse(this.localStorage.getDoubtDetail()); // Obtengo el detalle de la deuda desde local Storage
      if ( this.info.deuda ){
        this.deuda = this.info.deuda;
        this.deuda.monto = this.deuda.montoTotal;
        this.abonos = this.deuda.detalleAbonos;
        this.abonos.forEach(abono => {
          if ( this.deuda.currency == 'Bs.' ){
            abono.abonada.abonado = Number(abono.abonada.abonado/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          } else{
            abono.abonada.abonado = Number(abono.abonada.abonado/100).toLocaleString('en',  { minimumFractionDigits: 2 });
          }
        });
      } else{
        this.deuda = this.info;
      }
      console.log(this.deuda)
      if ( this.deuda.currency == 'usd' ){
        this.deuda.currency = '$';
      }
      console.log(this.deuda);
      this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0, 1)} ${Number(this.deuda.deudor.cedula.substring(1, this.deuda.deudor.cedula.length)).toLocaleString('de-DE', { minimumFractionDigits: 0 })}`;
      this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3, 6)} ${this.deuda.deudor.telefono.substring(6, 9)} ${this.deuda.deudor.telefono.substring(9, this.deuda.deudor.telefono.length)}`;
      let fechaLimite = new Date();
      fechaLimite.setTime(Date.parse(this.deuda.fechaLimite)); // obtengo la fecha limite de la deuda
      let fecha = new Date(); // obtengo fecha actual
      fecha.setHours(0, 0, 0, 0); // seteo la hora en 0
      let diff = fechaLimite.getTime() - fecha.getTime(); // calculo la diferencia en tiempo entre ambas fechas
      let days = Math.floor(diff / (1000 * 60 * 60 * 24)); // divido la fecha en ms para saber cuantos dias restantes hay
      if (this.deuda.estado !== 'deudaPagada' && days < 0) { // la deuda no ha sido pagada en el plazo por lo tanto se puede marcar como incobrable
        this.incobrable = true;
      }
      if ( !this.deuda.concepto ){
        this.deuda.concepto = 'Sin concepto'
      }
    }

  }

  goBack() {
    if ( this.info.deuda ){
      this.navCtrl.navigateBack('/tabs/pendientes/cobros-por-persona');
    } else{
      this.navCtrl.navigateBack('/tabs/pendientes');
    }
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }


  async answerDoubt(response) {
    // responde a la deuda
    console.log('Deuda ' + response);
    let dataResponse = {
      estado: response,
    };
    if (response == 'paga') {
      const appContext = {
        deudaCobrada: true,
      };
      this.localStorage.setContext(JSON.stringify(appContext));
      this.navCtrl.navigateRoot('tabs/pendientes/registro-pago');
    }else{
      this.presentLoading('Cargando...');
      (await this.businessService.markDoubtAs(this.deuda._id, dataResponse))
        .subscribe((resp: any) => {
          console.log('resp: ' + JSON.stringify(resp));
          setTimeout(() => {
            this.loading.dismiss();
            this.navCtrl.navigateRoot('tabs/pendientes');
          }, 1000);
        }, (err) => {
          console.log(err);
          setTimeout(() => {
            this.loading.dismiss();
          }, 1000);
        });
    }

  }

  deudaIncobrable() {
    // muestra aviso y llama a endpoint con respuesta negativa
    Swal.fire({
      imageUrl: './assets/images/icons/warning.svg',
      imageHeight: 80,
      imageWidth: 80,
      imageAlt: 'A tall image',
      text: 'Si marcas una deuda como incobrable será reflejada en tu historial',
      showCancelButton: false,
      showCloseButton: true,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Confirmar',
      heightAuto: false,
    }).then((result) => {
      if (result.value) {
        this.answerDoubt('incobrable');
      }
    });
  }

  eliminarDeuda() {
    // muestra aviso y llama a endpoint con respuesta negativa
    Swal.fire({
      imageUrl: './assets/images/icons/warning.svg',
      imageHeight: 80,
      imageWidth: 80,
      imageAlt: 'A tall image',
      text: '¿Deseas eliminar esta \
    solicitud de cobro?',
      showCancelButton: false,
      showCloseButton: true,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Confirmar',
      heightAuto: false,
    }).then((result) => {
      if (result.value) {
        this.answerDoubt('eliminada');
      }
    });
  }

}
