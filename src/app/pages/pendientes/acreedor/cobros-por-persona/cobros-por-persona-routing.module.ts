import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CobrosPorPersonaPage } from './cobros-por-persona.page';

const routes: Routes = [
  {
    path: '',
    component: CobrosPorPersonaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CobrosPorPersonaPageRoutingModule {}
