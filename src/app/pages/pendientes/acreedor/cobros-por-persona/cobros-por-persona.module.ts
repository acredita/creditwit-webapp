import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CobrosPorPersonaPageRoutingModule } from './cobros-por-persona-routing.module';

import { CobrosPorPersonaPage } from './cobros-por-persona.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CobrosPorPersonaPageRoutingModule
  ],
  declarations: [CobrosPorPersonaPage]
})
export class CobrosPorPersonaPageModule {}
