import { stringify } from 'querystring';
import { BusinessService } from 'src/app/services/business.service';
import { NavController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-rechazo-cobro',
  templateUrl: './rechazo-cobro.page.html',
  styleUrls: ['./rechazo-cobro.page.scss'],
})
export class RechazoCobroPage implements OnInit {

  deuda = {
    deudor: {
      nombres: '',
      apellidos: '',
      telefono: '',
      correo: '',
      cedula: '',
    },
    _id: ''
  };
  loading: any;
  incobrable: boolean = false;
  idDeuda: any;

  constructor(
    private navCtrl: NavController,
    private localStorage: StorageService,
    private businessService: BusinessService,
    private loadingCtrl: LoadingController,
    private route: ActivatedRoute,
  ) { }

  async ngOnInit() {
    // Obtengo la data que se va a mostrar.
    if ( this.localStorage.getDeudaRef() ){
      this.idDeuda = JSON.parse(this.localStorage.getDeudaRef()).idDeuda;
    } else{
      this.idDeuda = null;
    }
    if (this.idDeuda) {
      (await this.businessService.getDoubt(this.idDeuda))
        .subscribe((resp: any) => {
          console.log(resp);
          this.deuda = resp;
          this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0,1)} ${Number(this.deuda.deudor.cedula.substring(1,this.deuda.deudor.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 })}`;
          this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3,6)} ${this.deuda.deudor.telefono.substring(6,9)} ${this.deuda.deudor.telefono.substring(9,this.deuda.deudor.telefono.length)}`;
        }, (err) => {
          console.log(err);
        });
    }else{
      this.deuda = JSON.parse(this.localStorage.getDoubtDetail());
      this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0,1)} ${Number(this.deuda.deudor.cedula.substring(1,this.deuda.deudor.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 })}`;
      this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3,6)} ${this.deuda.deudor.telefono.substring(6,9)} ${this.deuda.deudor.telefono.substring(9,this.deuda.deudor.telefono.length)}`;
    }
    
    //console.log(this.deuda);

  }

  async ionViewWillEnter(){

    this.idDeuda = JSON.parse(this.localStorage.getDeudaRef());
    if (this.idDeuda) {
      this.presentLoading('Cargando');
      (await this.businessService.getDoubt(this.idDeuda.idDeuda))
        .subscribe((resp: any) => {
          console.log(resp);
          this.deuda = resp;
          this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0,1)} ${Number(this.deuda.deudor.cedula.substring(1,this.deuda.deudor.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 })}`;
          this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3,6)} ${this.deuda.deudor.telefono.substring(6,9)} ${this.deuda.deudor.telefono.substring(9,this.deuda.deudor.telefono.length)}`;
          setTimeout(() => {
            this.loading.dismiss();
          }, 1500);
          this.localStorage.removeDeudaRef();
        }, (err) => {
          console.log(err);
          this.localStorage.removeDeudaRef();
          setTimeout(() => {
            this.loading.dismiss();
          }, 1500);
          Swal.fire({
            text: err.error.error_description,
            confirmButtonColor: '#E7A016',
            confirmButtonText: 'Aceptar',
            heightAuto: false,
          }).then(async (result) => {
            this.navCtrl.navigateRoot('/tabs/pendientes');
          })
        });
    }else{
      this.deuda = JSON.parse(this.localStorage.getDoubtDetail());
      this.deuda.deudor.cedula = `${this.deuda.deudor.cedula.substring(0,1)} ${Number(this.deuda.deudor.cedula.substring(1,this.deuda.deudor.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 })}`;
      this.deuda.deudor.telefono = `0${this.deuda.deudor.telefono.substring(3,6)} ${this.deuda.deudor.telefono.substring(6,9)} ${this.deuda.deudor.telefono.substring(9,this.deuda.deudor.telefono.length)}`;
    }
    
    console.log(this.deuda);
  }

  goBack() {
    this.navCtrl.navigateBack('/tabs/pendientes');
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }


async userAcknowledged(){
  // confirma o rechaza el pago
  console.log('Acepto rechazo de deuda');
  console.log('deuda rechazada id: ' + this.deuda._id );
  this.presentLoading('Cargando...');
  (await this.businessService.rechazarDeuda(this.deuda._id))
    .subscribe((resp: any) => {
      console.log('resp: ' + JSON.stringify(resp));
      setTimeout(() => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('tabs/pendientes');
      }, 1000);
    }, (err) => {
      console.log(err);
      setTimeout(() => {
        this.loading.dismiss();
      }, 1000);

    });
}

}
