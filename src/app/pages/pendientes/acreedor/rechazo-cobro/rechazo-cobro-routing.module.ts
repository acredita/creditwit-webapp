import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RechazoCobroPage } from './rechazo-cobro.page';

const routes: Routes = [
  {
    path: '',
    component: RechazoCobroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RechazoCobroPageRoutingModule {}
