import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RechazoCobroPage } from './rechazo-cobro.page';

describe('RechazoCobroPage', () => {
  let component: RechazoCobroPage;
  let fixture: ComponentFixture<RechazoCobroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechazoCobroPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RechazoCobroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
