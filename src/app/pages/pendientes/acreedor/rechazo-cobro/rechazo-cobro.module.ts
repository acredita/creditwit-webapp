import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RechazoCobroPageRoutingModule } from './rechazo-cobro-routing.module';

import { RechazoCobroPage } from './rechazo-cobro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RechazoCobroPageRoutingModule
  ],
  declarations: [RechazoCobroPage]
})
export class RechazoCobroPageModule {}
