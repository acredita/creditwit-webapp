import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalificaDeudorPageRoutingModule } from './califica-deudor-routing.module';

import { CalificaDeudorPage } from './califica-deudor.page';
import { IonicRatingComponentModule } from 'ionic-rating-component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingComponentModule,
    CalificaDeudorPageRoutingModule
  ],
  declarations: [CalificaDeudorPage]
})
export class CalificaDeudorPageModule {}
