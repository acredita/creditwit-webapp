import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/services/business.service';
import { NavController, LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-califica-deudor',
  templateUrl: './califica-deudor.page.html',
  styleUrls: ['./califica-deudor.page.scss'],
})
export class CalificaDeudorPage implements OnInit {
  deuda: any;
  loading: any;
  incobrable: boolean = false;
  rating: any;


  constructor(
    private navCtrl: NavController,
    private localStorage: StorageService,
    private businessService: BusinessService,
    private loadingCtrl: LoadingController,
  ) { }

  ngOnInit() {
    // Obtengo la data que se va a mostrar.
    this.deuda = JSON.parse(this.localStorage.getDoubtDetail()); // Obtengo el detalle de la deuda desde local Storage
    console.log(this.deuda);
  }

  onRatingChange(event) {
    console.log('rating: ' + event);
    this.rating = event;
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  async finish() {
    // navigate root pendientes
    let data = {
      pago: this.deuda._id,
      user: this.deuda.deudor._id,
      rating: this.rating,
    }

    console.log('Data ' + JSON.stringify(data));

    this.presentLoading('Cargando...');
    (await this.businessService.calificaDeudor(data))
      .subscribe((resp: any) => {
        console.log('resp: ' + JSON.stringify(resp));
        setTimeout(() => {
          this.loading.dismiss();
          this.navCtrl.navigateRoot('tabs/pendientes');
        }, 1000);
      }, (err) => {
        console.log('Error: ' + JSON.stringify(err));
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000);

      });
  }




}
