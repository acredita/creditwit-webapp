import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalificaDeudorPage } from './califica-deudor.page';

describe('CalificaDeudorPage', () => {
  let component: CalificaDeudorPage;
  let fixture: ComponentFixture<CalificaDeudorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalificaDeudorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CalificaDeudorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
