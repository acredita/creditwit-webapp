import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalificaDeudorPage } from './califica-deudor.page';

const routes: Routes = [
  {
    path: '',
    component: CalificaDeudorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalificaDeudorPageRoutingModule {}
