import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.page.html',
  styleUrls: ['./pendientes.page.scss'],
})
export class PendientesPage implements OnInit {

  deudor: boolean;
  loading: any;
  porPagar = [];
  porCobrar = [];
  porPagarNotificaciones = [];
  deudaXConfirmar = [];
  pagoRechazado = [];
  porCobrarNotificaciones = [];
  deudaPagada = [];
  deudaRechazada = [];

  constructor(private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController) { }

  ngOnInit() {
    let userData = JSON.parse(this.storage.getUserData());
    if (userData.tipo == 'Acreedor') {
      this.deudor = false;
    }
    else {
      this.deudor = true;
    }
  }

  async ionViewWillEnter() {
    this.porPagar = [];
    this.porCobrar = [];
    this.porPagarNotificaciones = [];
    this.deudaXConfirmar = [];
    this.pagoRechazado = [];
    this.porCobrarNotificaciones = [];
    this.deudaPagada = [];
    this.deudaRechazada = [];
    console.log('ionViewWillEnter');
    const appContext = {
      deudaCobrada: false,
    };
    this.storage.setContext(JSON.stringify(appContext));
    this.storage.removeDeudaRef();
    (await this.businessService.getPendientes())
      .subscribe((resp: any) => {
        console.log(resp)
        this.porPagarNotificaciones = resp.porPagarNotificaciones;
        this.porPagar = resp.porPagar;
        this.porCobrarNotificaciones = resp.porCobrarNotificaciones;
        this.porCobrar = resp.porCobrar;
        this.orderPorPagar();
        this.orderPorCobrar();
      }, (err) => {
        console.log(err);
      });
  }

  async cobrar() {
    (await this.businessService.getPendientes())
      .subscribe((resp: any) => {
        this.porPagar = [];
        this.porCobrar = [];
        this.porPagarNotificaciones = [];
        this.deudaXConfirmar = [];
        this.pagoRechazado = [];
        this.porCobrarNotificaciones = [];
        this.deudaPagada = [];
        this.deudaRechazada = [];
        this.porPagarNotificaciones = resp.porPagarNotificaciones;
        this.porPagar = resp.porPagar;
        this.porCobrarNotificaciones = resp.porCobrarNotificaciones;
        this.porCobrar = resp.porCobrar;
        this.orderPorPagar();
        this.orderPorCobrar();
        console.log(resp)
      }, (err) => {
        console.log(err);
      });
    this.deudor = false;
  }

  async pagar() {
    (await this.businessService.getPendientes())
      .subscribe((resp: any) => {
        this.porPagar = [];
        this.porCobrar = [];
        this.porPagarNotificaciones = [];
        this.deudaXConfirmar = [];
        this.pagoRechazado = [];
        this.porCobrarNotificaciones = [];
        this.deudaPagada = [];
        this.deudaRechazada = [];
        this.porPagarNotificaciones = resp.porPagarNotificaciones;
        this.porPagar = resp.porPagar;
        this.porCobrarNotificaciones = resp.porCobrarNotificaciones;
        this.porCobrar = resp.porCobrar;
        this.orderPorPagar();
        this.orderPorCobrar();
        console.log(resp)
      }, (err) => {
        console.log(err);
      });
    this.deudor = true;
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  orderPorPagar() {
    this.deudaXConfirmar = []
    this.deudaRechazada = [];
    this.porPagarNotificaciones.forEach(element => {
      if (element.estado == "deudaXConfirmar") {
        element.monto = Number(element.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
        this.deudaXConfirmar.push(element);
      } else if (element.estado == "deudaPagada") {
        element.monto = Number(element.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      } else {
        element.monto = Number(element.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
        this.pagoRechazado.push(element);
      }
    });
    this.porPagar.forEach(element => {
      element.monto$ = Number(element.total$ / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      element.montoBs = Number(element.totalBs / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      element.cedula = `${element._id.substring(0, 1)} ${Number(
        element._id.substring(1, element._id.length)
      ).toLocaleString("de-DE")}`;

    });
    // this.porPagar.list.forEach(element => {
    //   if ( element._id == "deudaXConfirmar" ){
    //     this.porPagar.deudaXConfirmar = element.deudas;
    //     this.porPagar.deudaXConfirmar.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //     });
    //   }
    //   else if ( element._id == "deudaXPagar" ){
    //     this.porPagar.deudaXPagar = element.deudas;
    //     this.porPagar.deudaXPagar.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //       let fechaLimite = new Date;
    //       fechaLimite.setTime( Date.parse(deuda.fechaLimite) );
    //       let fecha = new Date;
    //       fecha.setHours(0,0,0,0);
    //       let diff = fechaLimite.getTime() - fecha.getTime();
    //       let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    //       deuda.days = days;
    //       if ( days >= 0 ){
    //         this.porPagar.porVencer.push( deuda );
    //       }
    //       else{
    //         this.porPagar.vencida.push( deuda );
    //       }
    //     });
    //   }
    //   else if ( element._id == "deudaPagada" ){
    //     this.porPagar.deudaPagada = element.deudas;
    //     this.porPagar.deudaPagada.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //       let fechaLimite = new Date;
    //       fechaLimite.setTime( Date.parse(deuda.fechaLimite) );
    //       let fecha = new Date;
    //       fecha.setHours(0,0,0,0);
    //       let diff = fechaLimite.getTime() - fecha.getTime();
    //       let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    //       deuda.days = days;
    //       this.porPagar.pagadas.push( deuda );
    //     });
    //   }
    //   else{
    //     this.porPagar.pagoRechazado = element.deudas;
    //     this.porPagar.pagoRechazado.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //     });
    //   }
    // });
  }

  orderPorCobrar() {
    this.deudaPagada = [];
    this.deudaRechazada = [];
    this.porCobrarNotificaciones.forEach(element => {
      if (element.estado == "pagoPendiente") {
        element.monto = Number(element.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
        this.deudaPagada.push(element);
      } else {
        element.monto = Number(element.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
        this.deudaRechazada.push(element);
      }
    });
    this.porCobrar.forEach(element => {
      element.monto$ = Number(element.total$ / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      element.montoBs = Number(element.totalBs / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      element.cedula = `${element._id.substring(0, 1)} ${Number(
        element._id.substring(1, element._id.length)
      ).toLocaleString("de-DE")}`;
    });
    // this.porCobrar.list.forEach(element => {
    //   if ( element._id == "deudaPagada" ){
    //     this.porCobrar.deudaPagada = element.deudas;
    //     this.porCobrar.deudaPagada.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //     });
    //   }
    //   else if ( element._id == "deudaXPagar" ){
    //     this.porCobrar.deudaXPagar = element.deudas;
    //     this.porCobrar.deudaXPagar.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //       let fechaLimite = new Date;
    //       fechaLimite.setTime( Date.parse(deuda.fechaLimite) );
    //       let fecha = new Date;
    //       fecha.setHours(0,0,0,0);
    //       let diff = fechaLimite.getTime() - fecha.getTime();
    //       let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    //       deuda.days = days;
    //       if ( days >= 0 ){
    //         this.porCobrar.porVencer.push( deuda );
    //       }
    //       else{
    //         this.porCobrar.vencida.push( deuda );
    //       }
    //     });
    //   }
    //   else{
    //     this.porCobrar.deudaRechazada = element.deudas;
    //     this.porCobrar.deudaRechazada.forEach(deuda => {
    //       if ( deuda.currency == 'usd' ){
    //         deuda.currency = '$';
    //       }
    //       deuda.monto = Number(deuda.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
    //     });
    //   }
    // });
  }

  goToDetalleDeuda(deuda) {
    this.storage.setDoubtDetail(JSON.stringify(deuda));
    this.navCtrl.navigateForward('/tabs/pendientes/detalle-deuda');
  }

  goToDetalleCobro(deuda) {
    this.storage.setDoubtDetail(JSON.stringify(deuda));
    this.navCtrl.navigateForward('/tabs/pendientes/detalle-cobro');
  }

  goToRechazoPago(deuda) {
    this.storage.setDoubtDetail(JSON.stringify(deuda));
    this.navCtrl.navigateForward('/tabs/pendientes/rechazar-pago');
  }

  goToPaymentDetail(payment) {
    this.storage.setDoubtDetail(JSON.stringify(payment));
    this.navCtrl.navigateForward('/tabs/pendientes/detalle-pago');
  }

  goToPaymentRejected(payment) {
    console.log("payment rejected");
    this.storage.setDoubtDetail(JSON.stringify(payment));
    this.navCtrl.navigateForward('/tabs/pendientes/rechazo-cobro');
  }

  goToListDeudas(deuda) {
    this.storage.setListDeudas(JSON.stringify(deuda));
    this.navCtrl.navigateForward('/tabs/pendientes/deudas-por-persona');
  }

  goToListCobros(deuda) {
    this.storage.setListDeudas(JSON.stringify(deuda));
    this.navCtrl.navigateForward('/tabs/pendientes/cobros-por-persona');
  }

}
