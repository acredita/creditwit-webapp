import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroPagoPage } from './registro-pago.page';

const routes: Routes = [
  {
    path: '',
    component: RegistroPagoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistroPagoPageRoutingModule {}
