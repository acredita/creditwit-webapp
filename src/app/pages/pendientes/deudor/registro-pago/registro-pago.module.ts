import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistroPagoPageRoutingModule } from './registro-pago-routing.module';

import { RegistroPagoPage } from './registro-pago.page';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroPagoPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [RegistroPagoPage]
})
export class RegistroPagoPageModule {}
