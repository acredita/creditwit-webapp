import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, NavController, ActionSheetController, Platform } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import Swal from "sweetalert2";

declare var window: any;

@Component({
  selector: 'app-registro-pago',
  templateUrl: './registro-pago.page.html',
  styleUrls: ['./registro-pago.page.scss'],
})
export class RegistroPagoPage implements OnInit {

  deuda: any;
  pagoForm: FormGroup;
  metodoInvalid: boolean;
  cuentas: any;
  account = [];
  id_cuenta: any;
  context: any;
  customPopoverOptions: any = {
    'cssClass': 'popover-select'
  };
  loading: HTMLIonLoadingElement;
  upload: boolean = false;
  textImage = 'Imagen de la transacción';
  file: File;
  blob: Blob;
  fileToUpload: File;
  fixedAmount: any;
  amountInvalid: boolean;
  idInvalid: boolean;
  preventDefault: boolean;
  amountCurrency;

  precision = 2;

  entry;

  constructor(private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
    public fb: FormBuilder,
    private camera: Camera,
    private actionSheetCtrl: ActionSheetController,
    private platform: Platform) { }

  ngOnInit() {
    this.amountCurrency = '';
    this.pagoForm = this.fb.group({
      metodo: ['', [Validators.required]],
      referencia: [''],
      currencyType: ['$', [Validators.required]],
      amount: ['', [Validators.required]]
    });
    this.textImage = 'Imagen de la transacción';
    this.deuda = JSON.parse(this.storage.getDoubtDetail());
    console.log(this.deuda);
    this.context = JSON.parse(this.storage.getContext());
    console.log(' context: ' + JSON.stringify(this.context));
    if (this.context.deudaCobrada) { // el mismo acreedor marca la deuda como cobrada
      let userData = JSON.parse(this.storage.getUserData());
      this.cuentas = userData.payMethods;
    }
    else {
      this.cuentas = JSON.parse(this.storage.getCuentas());
    }
    if (!this.deuda.concepto) {
      this.deuda.concepto = 'Sin concepto'
    }
    this.cuentas.forEach(cuenta => {
      let account = {
        id: '',
        titulo: ''
      };
      if (cuenta.metodo == 'Transferencia') {
        account.titulo = `${cuenta.detalle.banco} ${cuenta.detalle.numero}`;
        account.id = cuenta._id;
      }
      if (cuenta.metodo == 'Zelle') {
        account.titulo = `Zelle ${cuenta.detalle.correo}`;
        account.id = cuenta._id;
      }
      if (cuenta.metodo == 'PagoMovil') {
        let numero = cuenta.detalle.numero.replace('+58', '0');
        numero = `${numero.substring(0, 4)} ${numero.substring(4, 7)} ${numero.substring(7, 11)}`;
        account.titulo = `${cuenta.detalle.banco} ${numero}`;
        cuenta.detalle.numero = numero.replace('0', '+58').replace(' ', '').replace(' ', '');
        account.id = cuenta._id;
      }
      if (cuenta.metodo == 'Efectivo') {
        account.titulo = 'Efectivo';
        account.id = cuenta._id;
      }
      if (cuenta.metodo == 'Otro') {
        account.titulo = cuenta.detalle.titulo;
        account.id = cuenta._id;
      }
      this.account.push(account);
    });
    console.log(this.account);
    if (this.deuda.deudas[0].deudor._id == this.deuda.clientId) {
      this.context.deudaCobrada = true;
      this.storage.setContext(JSON.stringify(this.context));
    }
  }

  async fetchAccounts() {
    (await this.businessService.getCuentasAcreedor(this.deuda.acreedor._id))
      .subscribe((resp: any) => {
        console.log('cuentas bancarias: ' + JSON.stringify(resp));
        this.storage.setCuentas(JSON.stringify(resp.payMethods));
      }, (err) => {
        console.log(err);
      });
  }

  goBack() {
    this.navCtrl.back();
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  async onContinue() {
    try {
      const form = this.pagoForm.value;
      this.metodoInvalid = false;

      if (form.amount == "" || form.amount == "0" || form.amount == undefined) {
        this.amountInvalid = true;
      } else {
        this.amountInvalid = false;
      }

      if (form.metodo == '') {
        this.metodoInvalid = true;
      }
      this.getMetodo(form.metodo);
      if (this.pagoForm.valid) {
        let amountForAPI: any = form.amount;
        let tipoMoneda;
        if (form.currencyType.toString().toLowerCase() == "bs") {
          amountForAPI = amountForAPI.replace(/\./g, "").replace(",", ".");
          tipoMoneda = "bs";
          amountForAPI = Math.round(parseFloat(amountForAPI) * 100);
          if (amountForAPI > this.deuda.totalBs) {
            Swal.fire({
              confirmButtonColor: "#E7A016",
              text: 'El monto a abonar no puede ser mayor a la deuda',
              heightAuto: false,
              showCloseButton: true
            });
            return
          }
        } else {
          tipoMoneda = "usd";
          amountForAPI = amountForAPI.replace(/\./g, "").replace(",", ".");
          amountForAPI = Math.round(parseFloat(amountForAPI) * 100);
          if (amountForAPI > this.deuda.total$) {
            Swal.fire({
              confirmButtonColor: "#E7A016",
              text: 'El monto a abonar no puede ser mayor a la deuda',
              heightAuto: false,
              showCloseButton: true
            });
            return
          }
        }
        if (this.upload) {
          var formData: FormData = new FormData();
          formData.append('metodo', JSON.stringify(this.id_cuenta));
          formData.append('referencia', form.referencia);
          formData.append('comprobante', this.fileToUpload, this.fileToUpload.name);
          formData.append('monto', amountForAPI);
          formData.append('currency', form.currencyType.toString().toLowerCase());
          formData.append('acreedor', this.deuda.deudas[0].acreedor._id);
          formData.append('deudorId', this.deuda.deudas[0].deudor._id != this.deuda.clientId ? '' : this.deuda.deudas[0].deudor._id);
          formData.append('deudorCedula', this.deuda.deudas[0].deudor.cedula);

          for (var key of formData['entries']()) {
            //alert(key[0] + ', ' + key[1]);
            console.log(key[1])
          }
          this.presentLoading('Cargando');
          (await this.businessService.registrarPago(formData))
            .subscribe((resp: any) => {
              console.log(resp);
              let pago = {
                metodo: this.id_cuenta,
                deuda: this.deuda,
                pago: resp,
                montoAbonado: form.amount
              }
              this.storage.setPago(JSON.stringify(pago));
              setTimeout(() => {
                this.loading.dismiss();
              }, 1000);
              this.navCtrl.navigateRoot('/tabs/pendientes/confirmar-pago');
            }, (err) => {
              console.log(err);
              setTimeout(() => {
                this.loading.dismiss();
              }, 1000);
            });
        }
        else {
          var formData = new FormData();
          formData.append('metodo', JSON.stringify(this.id_cuenta));
          formData.append('referencia', form.referencia);
          formData.append('monto', amountForAPI);
          formData.append('currency', form.currencyType.toString().toLowerCase());
          formData.append('acreedor', this.deuda.deudas[0].acreedor._id);
          formData.append('deudorId', this.deuda.deudas[0].deudor._id != this.deuda.clientId ? '' : this.deuda.deudas[0].deudor._id);
          formData.append('deudorCedula', this.deuda.deudas[0].deudor.cedula);

          this.presentLoading('Cargando');
          (await this.businessService.registrarPago(formData))
            .subscribe((resp: any) => {
              console.log(resp)
              let pago = {
                metodo: this.id_cuenta,
                deuda: this.deuda,
                pago: resp,
                montoAbonado: form.amount,
                currency: form.currencyType
              }
              this.storage.setPago(JSON.stringify(pago));
              setTimeout(() => {
                this.loading.dismiss();
              }, 1000);
              this.navCtrl.navigateRoot('/tabs/pendientes/confirmar-pago');
            }, (err) => {
              console.log(err);
              setTimeout(() => {
                this.loading.dismiss();
              }, 1000);
            });
        }
      }
    } catch (err) {
      alert(err)
    }
  }

  get errorControl() {
    return this.pagoForm.controls;
  }

  getMetodo(id) {
    this.cuentas.forEach(cuenta => {
      if (cuenta._id == id) {
        this.id_cuenta = cuenta;
      }
    });
  }

  async uploadImage() {
    if (this.platform.is('cordova')) {
      const actionSheet = await this.actionSheetCtrl.create({
        backdropDismiss: true,
        buttons: [{
          text: 'Cámara',
          icon: 'camera',
          handler: () => {
            this.camara();
          }
        }, {
          text: 'Galería',
          icon: 'image',
          handler: () => {
            this.galeria();
          }
        }, {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }
  }

  deleteImage() {
    this.upload = false;
    this.textImage = 'Imagen de la transacción';
  }

  camara() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      correctOrientation: true
    }
    this.getPicture(options);
  }

  galeria() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }
    this.getPicture(options);
  }

  getPicture(options: CameraOptions) {
    this.camera.getPicture(options).then((imageData) => {
      alert(imageData)
      let blob = this.getBlob(imageData, "image/jpg")
      alert(blob)
      this.blob = blob;
      const file = new File([blob], "image.jpg")
      this.file = file;
      this.upload = true;
      this.textImage = 'Imagen cargada';
    }, (err) => {
      // Handle error
      this.upload = false;
      this.textImage = 'Imagen de la transacción';
    });
  }

  getBlob(b64Data: string, contentType: string, sliceSize: number = 512) {
    try {
      const byteCharacters = atob(b64Data);
      const byteArrays = [];

      for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      const blob = new Blob(byteArrays, { type: contentType });
      return blob;
    }
    catch (err) {
      alert(err)
    }
  }

  handleFileInput(files: FileList) {
    console.log(files)
    this.fileToUpload = files.item(0);
    this.textImage = this.fileToUpload.name;
    this.upload = true;
  }

  clearAmount() {
    const form = this.pagoForm.value;
    if (form.amount != 0 && form.amount != "" && form.amount != undefined) {
      if (form.currencyType == "Bs") {
        this.fixedAmount = form.amount
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "");
        if (this.fixedAmount.toString().includes(".")) {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          });
        } else {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("de-DE");
        }
      } else {
        this.fixedAmount = form.amount
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(",", ".");
        if (this.fixedAmount.toString().includes(".")) {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("en", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          });
        } else {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("en");
        }
      }
    }
    this.pagoForm.patchValue({ amount: this.fixedAmount });
    this.amountInvalid = false;
    this.idInvalid = false;
  }

  onChange(event: any) {
    this.preventDefault = false;
    //console.log(event)
    let value: string = event.target.value;
    //console.log(value);
    if (value !== "") {
      if (this.pagoForm.value.currencyType.toLowerCase() == "bs") {
        // FORMATO EN BOLIVARES
        if (value.includes(",")) {
          const regex = /[0-9]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        } else {
          const regex = /[0-9,","]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        }
      } else {
        // FORMATO EN DOLARES
        console.log("escribiendo en usd");
        if (value.includes(".")) {
          let regex = /[0-9]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        } else {
          let regex = /[0-9 "."]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        }
      }
    }
  }

  afterTextChanged(event: any) {
    this.cleanInputError();
    let value = event.target.value;
    if (value != "") {
      if (this.pagoForm.value.currencyType.toLowerCase() == "bs") {
        // FORMATO EN BOLIVARES
        this.fixedAmount = value
          .toString()
          .replace(/\./g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      } else {
        // FORMATO EN DOLARES
        this.fixedAmount = value
          .toString()
          .replace(/,/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    } else {
      this.fixedAmount = "";
    }

    this.pagoForm.patchValue({ amount: this.fixedAmount });
  }

  cleanInputError() {
    this.idInvalid = false;
    this.amountInvalid = false;
  }

  amountChanged(event: number) {
    this.pagoForm.patchValue({
      amount: (event / Math.pow(10, this.precision)).toLocaleString("de-DE"),
    });
    this.amountCurrency = event;
  }

}
