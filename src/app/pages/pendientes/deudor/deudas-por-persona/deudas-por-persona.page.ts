import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from '../../../../services/storage.service';

@Component({
  selector: 'app-deudas-por-persona',
  templateUrl: './deudas-por-persona.page.html',
  styleUrls: ['./deudas-por-persona.page.scss'],
})
export class DeudasPorPersonaPage implements OnInit {

  deuda:any;
  pagosVencidos = [];
  pagosPorVencer = [];
  cuentas:any = null;

  constructor( private navCtrl: NavController,
    private platform: Platform,
    private storage: StorageService,
    private businessService: BusinessService ) { }

  ngOnInit() {
    if ( this.platform.is('ios') && this.platform.is('cordova') ){
      document.getElementById('content-principal-deuda-por-persona').classList.add('content-principal-ios');
    }
    this.deuda = JSON.parse( this.storage.getListDeudas() );
  }

  ionViewWillEnter(){
    this.pagosVencidos = [];
    this.pagosPorVencer = [];
    this.deuda = JSON.parse( this.storage.getListDeudas() );
    console.log(this.deuda)
    this.orderDeudas();
    this.cuentas = null;
    this.getCuentas();
  }

  orderDeudas(){
    this.deuda.deudas.forEach(element => {
      if ( element.currency == 'Bs.' ){
        element.montoTotal = Number(element.monto/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
        if ( element.restante ){
          element.restanteTotal = Number(element.restante/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
        }
      } else{
        element.montoTotal = Number(element.monto/100).toLocaleString('en',  { minimumFractionDigits: 2 });
        if ( element.restante ){
          element.restanteTotal = Number(element.restante/100).toLocaleString('en',  { minimumFractionDigits: 2 });
        }
      }
      if ( element.dias > 0 ){
        this.pagosPorVencer.push( element )
      } else{
        this.pagosVencidos.push( element );
      }
    });
  }

  goBack(){
    this.navCtrl.navigateBack('/tabs/pendientes');
  }

  goToDetalleDeuda( deuda:any ){
    let data = {
      deuda,
      info: this.deuda
    }
    this.storage.setDoubtDetail( JSON.stringify( data ) );
    this.navCtrl.navigateForward( '/tabs/pendientes/detalle-deuda' );
  }

  async getCuentas(){
    (await this.businessService.getCuentasAcreedor(this.deuda.deudas[0].acreedor._id))
    .subscribe( (resp : any) => {
      console.log(resp)
      this.cuentas = resp.payMethods;
    }, (err) => {
      console.log(err);
    });
  }

  registrarPago(){
    this.storage.setCuentas( JSON.stringify(this.cuentas) );
    this.storage.setDoubtDetail( JSON.stringify( this.deuda ) );
    this.navCtrl.navigateForward('/tabs/pendientes/registro-pago');
  }

}
