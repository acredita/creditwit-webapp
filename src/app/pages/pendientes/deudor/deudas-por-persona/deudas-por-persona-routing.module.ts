import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeudasPorPersonaPage } from './deudas-por-persona.page';

const routes: Routes = [
  {
    path: '',
    component: DeudasPorPersonaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeudasPorPersonaPageRoutingModule {}
