import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeudasPorPersonaPageRoutingModule } from './deudas-por-persona-routing.module';

import { DeudasPorPersonaPage } from './deudas-por-persona.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeudasPorPersonaPageRoutingModule
  ],
  declarations: [DeudasPorPersonaPage]
})
export class DeudasPorPersonaPageModule {}
