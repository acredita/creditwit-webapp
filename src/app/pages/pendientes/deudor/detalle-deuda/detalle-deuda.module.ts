import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleDeudaPageRoutingModule } from './detalle-deuda-routing.module';

import { DetalleDeudaPage } from './detalle-deuda.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleDeudaPageRoutingModule
  ],
  declarations: [DetalleDeudaPage]
})
export class DetalleDeudaPageModule {}
