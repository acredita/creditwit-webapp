import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, Platform } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-deuda',
  templateUrl: './detalle-deuda.page.html',
  styleUrls: ['./detalle-deuda.page.scss'],
})
export class DetalleDeudaPage implements OnInit {

  deuda:any;
  info:any;
  porConfirmar: boolean = false;
  loading: any;
  cuentas = [];
  idDeuda: any;
  metodoPagoLabel: string = 'método de pago';
  abonos = [];
  constructor( private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private platform: Platform
    ) { }

  async ngOnInit() {
    if ( this.platform.is('ios') && this.platform.is('cordova') ){
      document.getElementById('content-principal-detalle-deuda').classList.add('content-principal-ios');
    }
    this.idDeuda = JSON.parse( this.storage.getDeudaRef() );
    console.log(this.idDeuda);
    if (this.idDeuda) { // ruta publica entonces debe consultar el detalle de la deuda
      (await this.businessService.getDoubt(this.idDeuda.idDeuda))
        .subscribe((resp: any) => {
          console.log(resp);
          this.deuda = resp;
          if ( this.deuda.currency == 'usd' ){
            this.deuda.currency = '$';
          }
          this.deuda.monto = (parseFloat(this.deuda.monto)/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          if ( !this.deuda.concepto ){
            this.deuda.concepto = 'Sin concepto'
          }
          this.setearDeuda();
          this.storage.setDoubtDetail( JSON.stringify( this.deuda ) );
          this.storage.removeDeudaRef();
        }, (err) => {
          console.log(err);
          this.storage.removeDeudaRef();
          Swal.fire({
            text: err.error.error_description,
            confirmButtonColor: '#E7A016',
            confirmButtonText: 'Aceptar',
            heightAuto: false,
          }).then(async (result) => {
            this.navCtrl.navigateRoot('/tabs/pendientes');
          })
        });
    }else{
      this.info = JSON.parse(this.storage.getDoubtDetail()); // esta dentro de la app entonces consulta la info de deuda en localStorage
      if ( this.info.deuda ){
        this.deuda = this.info.deuda;
        this.deuda.monto = this.deuda.montoTotal;
        this.abonos = this.deuda.detalleAbonos;
        this.abonos.forEach(abono => {
          if ( this.deuda.currency == 'Bs.' ){
            abono.abonada.abonado = Number(abono.abonada.abonado/100).toLocaleString('de-DE',  { minimumFractionDigits: 2 });
          } else{
            abono.abonada.abonado = Number(abono.abonada.abonado/100).toLocaleString('en',  { minimumFractionDigits: 2 });
          }
        });
      } else{
        this.deuda = this.info;
      }
      console.log(this.deuda)
      if ( !this.deuda.concepto ){
        this.deuda.concepto = 'Sin concepto'
      }
      this.setearDeuda();
    }
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  goBack(){
    if ( this.info.deuda ){
      this.navCtrl.navigateBack('/tabs/pendientes/deudas-por-persona');
    } else{
      this.navCtrl.navigateBack('/tabs/pendientes');
    }
  }

  async answerDoubt( event ){
    let data = {
      codigo: this.deuda.activacion,
      acepta: event
    }
    if ( event ){
      this.presentLoading( 'Cargando' );
      (await this.businessService.answerDoubt(data))
      .subscribe( (resp : any) => {
        console.log(resp)
        this.navCtrl.navigateRoot('/tabs/pendientes');
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000 );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000 );
      });
    }
    else{
      Swal.fire({
        imageUrl: './assets/images/icons/warning.svg',
        imageHeight: 80,
        imageWidth: 80,
        imageAlt: 'A tall image',
        text: '¿Deseas rechazar esta solicitud de cobro?',
        showCancelButton: false,
        showCloseButton: true,
        confirmButtonColor: '#E7A016',
        confirmButtonText: 'Confirmar',
        heightAuto: false,
      }).then(async (result) => {
        if (result.value) {
          this.presentLoading( 'Cargando' );
          (await this.businessService.answerDoubt(data))
          .subscribe( (resp : any) => {
            console.log(resp)
            this.navCtrl.navigateRoot('/tabs/pendientes');
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          }, (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          });
        }
      })
    }
  }

  goToCuentas(){
    if ( this.cuentas.length > 0 ){
      this.storage.setCuentas( JSON.stringify( this.cuentas ) )
      this.navCtrl.navigateForward( '/tabs/pendientes/datos-deuda' );
    }
  }

  registrarPago(){
    this.storage.setCuentas( JSON.stringify(this.cuentas) );
    this.navCtrl.navigateForward('/tabs/pendientes/registro-pago');
  }

  async setearDeuda(){
    this.deuda.acreedor.cedula = `${this.deuda.acreedor.cedula.substring(0,1)} ${Number(this.deuda.acreedor.cedula.substring(1,this.deuda.acreedor.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 })}`;
    this.deuda.acreedor.telefono = `0${this.deuda.acreedor.telefono.substring(3,6)} ${this.deuda.acreedor.telefono.substring(6,9)} ${this.deuda.acreedor.telefono.substring(9,this.deuda.acreedor.telefono.length)}`;
    (await this.businessService.getCuentasAcreedor(this.deuda.acreedor._id))
    .subscribe( (resp : any) => {
      console.log(resp)
      this.cuentas = resp.payMethods;
      this.metodoPagoLabel = this.cuentas.length > 1 ? 'métodos de pago' : 'método de pago';
      setTimeout(() => {
        // this.loading.dismiss();
      }, 500 );
    }, (err) => {
      console.log(err);
    });
    if (this.deuda.estado == "deudaXConfirmar"){
      this.porConfirmar = true;
    }
    else{
      this.porConfirmar = false;
    }
  }

}
