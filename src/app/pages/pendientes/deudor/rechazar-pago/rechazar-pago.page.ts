import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-rechazar-pago',
  templateUrl: './rechazar-pago.page.html',
  styleUrls: ['./rechazar-pago.page.scss'],
})
export class RechazarPagoPage implements OnInit {

  deuda = {
    acreedor: {
      nombres: '',
      apellidos: '',
      telefono: '',
      correo: ''
    },
    _id: ''
  };
  loading: HTMLIonLoadingElement;
  idDeuda: any;

  constructor(private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
  ) {
  }

  async ngOnInit() {
    this.idDeuda = JSON.parse(this.storage.getDeudaRef());
    console.log(this.idDeuda)
    if (this.idDeuda) { // ruta publica entonces debe consultar el detalle de la deuda
      (await this.businessService.getDoubt(this.idDeuda.idDeuda))
        .subscribe((resp: any) => {
          console.log('DEUDA' + JSON.stringify(resp));
          this.deuda = resp;
        }, (err) => {
          console.log(err);
        });
    } else {
      this.deuda = JSON.parse(this.storage.getDoubtDetail()); // esta dentro de la app entonces consulta la info de deuda en localStorage
    }

    this.deuda.acreedor.telefono = `0${this.deuda.acreedor.telefono.substring(3, 6)} ${this.deuda.acreedor.telefono.substring(6, 9)} ${this.deuda.acreedor.telefono.substring(9, this.deuda.acreedor.telefono.length)}`;
    //console.log(this.deuda)
  }

  async ionViewWillEnter() {
    this.idDeuda = JSON.parse(this.storage.getDeudaRef());
    if (this.idDeuda) { // ruta publica entonces debe consultar el detalle de la deuda
      this.presentLoading('Cargando');
      (await this.businessService.getDoubt(this.idDeuda.idDeuda))
        .subscribe((resp: any) => {
          console.log('DEUDA' + JSON.stringify(resp));
          this.deuda = resp;
          setTimeout(() => {
            this.loading.dismiss();
          }, 1500);
          this.storage.removeDeudaRef();
        }, (err) => {
          console.log(err);
          this.storage.removeDeudaRef();
          setTimeout(() => {
            this.loading.dismiss();
          }, 1500);
          Swal.fire({
            text: err.error.error_description,
            confirmButtonColor: '#E7A016',
            confirmButtonText: 'Aceptar',
            heightAuto: false,
          }).then(async (result) => {
            this.navCtrl.navigateRoot('/tabs/pendientes');
          })
        });
    } else {
      this.deuda = JSON.parse(this.storage.getDoubtDetail()); // esta dentro de la app entonces consulta la info de deuda en localStorage
    }

    this.deuda.acreedor.telefono = `0${this.deuda.acreedor.telefono.substring(3, 6)} ${this.deuda.acreedor.telefono.substring(6, 9)} ${this.deuda.acreedor.telefono.substring(9, this.deuda.acreedor.telefono.length)}`;
    console.log(this.deuda)
  }

  goBack() {
    this.navCtrl.navigateBack('/tabs/pendientes');
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  async onContinue() {
    this.presentLoading('Cargando');
    (await this.businessService.doubtRefusedAcknowledge(this.deuda._id))
      .subscribe((resp: any) => {
        console.log(resp)
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000);
        this.navCtrl.navigateRoot('/tabs/pendientes');
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000);
      });
  }

}
