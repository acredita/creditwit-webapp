import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RechazarPagoPage } from './rechazar-pago.page';

const routes: Routes = [
  {
    path: '',
    component: RechazarPagoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RechazarPagoPageRoutingModule {}
