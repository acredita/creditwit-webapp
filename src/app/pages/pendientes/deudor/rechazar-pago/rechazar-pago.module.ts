import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RechazarPagoPageRoutingModule } from './rechazar-pago-routing.module';

import { RechazarPagoPage } from './rechazar-pago.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RechazarPagoPageRoutingModule
  ],
  declarations: [RechazarPagoPage]
})
export class RechazarPagoPageModule {}
