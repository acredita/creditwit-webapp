import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-confirmar-pago',
  templateUrl: './confirmar-pago.page.html',
  styleUrls: ['./confirmar-pago.page.scss'],
})
export class ConfirmarPagoPage implements OnInit {

  pago: any;
  context: any; // obj para saber si debo navegar a calificar usuario o a pendientes
  currencyDate = new Date();
  deudasPagas = [];

  constructor(private storage: StorageService,
    private navCtrl: NavController) { }

  ngOnInit() {
    this.pago = JSON.parse(this.storage.getPago());

    if (this.pago.currency == 'usd') {
      this.pago.currency = '$';
    }
    this.pago.montoAbonado = this.pago.montoAbonado.replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace('.', '').replace(',', '.')
    if (this.pago.currency == 'Bs.') {
      this.pago.montoAbonado = Number(this.pago.montoAbonado).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      if (this.pago.pago.deudaAbonada.abonado) {
        this.pago.pago.deudaAbonada.abonado = Number(this.pago.pago.deudaAbonada.abonado / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      }
    } else {
      this.pago.montoAbonado = Number(this.pago.montoAbonado).toLocaleString('en', { minimumFractionDigits: 2 });
      if (this.pago.pago.deudaAbonada.abonado) {
        this.pago.pago.deudaAbonada.abonado = Number(this.pago.pago.deudaAbonada.abonado / 100).toLocaleString('en', { minimumFractionDigits: 2 });
      }
    }
    if (this.pago.pago.deudaAbonada.concepto == '') {
      this.pago.pago.deudaAbonada.concepto = 'Sin concepto';
    }
    this.deudasPagas = this.pago.pago.deudasPagas;
    this.deudasPagas.forEach(pago => {
      if (this.pago.currency == 'Bs.') {
        pago.monto = Number(pago.monto / 100).toLocaleString('de-DE', { minimumFractionDigits: 2 });
      } else {
        pago.monto = Number(pago.monto / 100).toLocaleString('en', { minimumFractionDigits: 2 });
      }
      if (pago.concepto == '') {
        pago.concepto = 'Sin concepto';
      }
    });
    // if (this.pago.metodo.metodo == 'Transferencia'){
    //   this.pago.metodo.detalle.cedula = `${this.pago.metodo.detalle.cedula.substring(0,1)} ${ Number(this.pago.metodo.detalle.cedula.substring(1, this.pago.metodo.detalle.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 }) }`;
    // }
    // if (this.pago.metodo.metodo == 'PagoMovil'){
    //   this.pago.metodo.detalle.numero = this.pago.metodo.detalle.numero.replace('+58', '0');
    //   this.pago.metodo.detalle.numero = `${this.pago.metodo.detalle.numero.substring(0,4)} ${this.pago.metodo.detalle.numero.substring(4,7)} ${this.pago.metodo.detalle.numero.substring(7,11)}`;
    //   this.pago.metodo.detalle.cedula = `${this.pago.metodo.detalle.cedula.substring(0,1)} ${ Number(this.pago.metodo.detalle.cedula.substring(1, this.pago.metodo.detalle.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 }) }`;
    //   this.pago.metodo.metodo = 'Pago Móvil';
    // }
    console.log(this.pago)
  }

  onContinue() {
    this.context = JSON.parse(this.storage.getContext()); // Obtengo info sobre el contexto del registro del pago
    console.log(' context: ' + JSON.stringify(this.context));
    if (this.context.deudaCobrada) {
      /** si el contexto proviene de una deuda que el acreedor marco como cobrada,
        ya registro el pago ahora debe calificar al deudor */
      let deuda = JSON.parse(this.storage.getDoubtDetail());
      deuda.deudas[0]._id = this.pago.pago.pagoId;
      this.storage.setDoubtDetail(JSON.stringify(deuda.deudas[0]));
      this.navCtrl.navigateRoot('/tabs/califica-deudor');
    } else {
      this.navCtrl.navigateRoot('/tabs/pendientes');
    }

  }

}
