import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatosDeudaPageRoutingModule } from './datos-deuda-routing.module';

import { DatosDeudaPage } from './datos-deuda.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosDeudaPageRoutingModule
  ],
  declarations: [DatosDeudaPage]
})
export class DatosDeudaPageModule {}
