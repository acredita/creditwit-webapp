import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatosDeudaPage } from './datos-deuda.page';

const routes: Routes = [
  {
    path: '',
    component: DatosDeudaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatosDeudaPageRoutingModule {}
