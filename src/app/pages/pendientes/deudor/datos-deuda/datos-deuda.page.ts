import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-datos-deuda',
  templateUrl: './datos-deuda.page.html',
  styleUrls: ['./datos-deuda.page.scss'],
})
export class DatosDeudaPage implements OnInit {

  cuentas = [];

  constructor( private storage: StorageService,
    private navCtrl: NavController,
    private platform: Platform ) { }

  ngOnInit() {
    if ( this.platform.is('ios') && this.platform.is('cordova') ){
      document.getElementById('content-principal-datos-deuda').classList.add('content-principal-ios');
    }
    this.cuentas = JSON.parse(this.storage.getCuentas());
    this.cuentas.forEach(cuenta => {
      if (cuenta.metodo == 'Transferencia'){
        cuenta.titulo = 'Cuenta bancaria';
        cuenta.detalle.cedula = `${cuenta.detalle.cedula.substring(0,1)} ${ Number(cuenta.detalle.cedula.substring(1, cuenta.detalle.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 }) }`;
      }
      if (cuenta.metodo == 'Zelle'){
        cuenta.titulo = 'Zelle';
        if ( cuenta.detalle.correo.includes('@') ){
          cuenta.detalle.correo = cuenta.detalle.correo;
        }
        else{
          cuenta.detalle.correo = `(${cuenta.detalle.correo.substring(0,3)}) ${cuenta.detalle.correo.substring(3,6)} ${cuenta.detalle.correo.substring(6,10)}`;
        }
      }
      if (cuenta.metodo == 'PagoMovil'){
        cuenta.titulo = 'Pago móvil';
        cuenta.detalle.numero = cuenta.detalle.numero.replace('+58', '0');
        cuenta.detalle.numero = `${cuenta.detalle.numero.substring(0,4)} ${cuenta.detalle.numero.substring(4,7)} ${cuenta.detalle.numero.substring(7,11)}`;
        cuenta.detalle.cedula = `${cuenta.detalle.cedula.substring(0,1)} ${ Number(cuenta.detalle.cedula.substring(1, cuenta.detalle.cedula.length)).toLocaleString('de-DE',  { minimumFractionDigits: 0 }) }`;
      }
      if (cuenta.metodo == 'Efectivo'){
        cuenta.titulo = 'Efectivo';
      }
      if (cuenta.metodo == 'Otro'){
        cuenta.titulo = 'Otro';
      }
    });
  }

  goBack(){
    this.navCtrl.navigateBack('/tabs/pendientes/detalle-deuda');
  }

}
