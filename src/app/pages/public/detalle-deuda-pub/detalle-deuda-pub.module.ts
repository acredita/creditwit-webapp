import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleDeudaPubPageRoutingModule } from './detalle-deuda-pub-routing.module';

import { DetalleDeudaPubPage } from './detalle-deuda-pub.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleDeudaPubPageRoutingModule
  ],
  declarations: [DetalleDeudaPubPage]
})
export class DetalleDeudaPubPageModule {}
