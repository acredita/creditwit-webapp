import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detalle-deuda-pub',
  templateUrl: './detalle-deuda-pub.page.html',
  styleUrls: ['./detalle-deuda-pub.page.scss'],
})
export class DetalleDeudaPubPage implements OnInit {

  idDeuda: any;
  deuda: any;
  porConfirmar: boolean = false;
  loading: any;
  cuentas = [];
 

  constructor(
    private storage: StorageService,
    private route: ActivatedRoute,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
  ) { }

  async ngOnInit() {
    this.idDeuda = this.route.snapshot.paramMap.get('id');

    this.presentLoading( 'Cargando' );
    (await this.businessService.getDoubt(this.idDeuda))
    .subscribe( (resp : any) => {
      console.log('DEUDA' + JSON.stringify(resp));
      this.deuda = resp;
      setTimeout(() => {
        this.loading.dismiss();
      }, 1500 );
    }, (err) => {
      console.log(err);
      setTimeout(() => {
        this.loading.dismiss();
      }, 1500 );
    });
    if (this.deuda.estado == "deudaXConfirmar"){
      this.porConfirmar = true;
    }
    else{
      this.porConfirmar = false;
    }
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }


  async answerDoubt( event ){
    let data = {
      codigo: this.deuda.activacion,
      acepta: event
    }
    if ( event ){
      this.presentLoading( 'Cargando' );
      (await this.businessService.answerDoubt(data))
      .subscribe( (resp : any) => {
        console.log(resp);
        // this.navCtrl.navigateRoot('/tabs/pendientes');
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      });
    }
    else{
      Swal.fire({
        imageUrl: './assets/images/icons/warning.svg',
        imageHeight: 80,
        imageWidth: 80,
        imageAlt: 'A tall image',
        text: '¿Deseas rechazar esta solicitud de cobro?',
        showCancelButton: false,
        showCloseButton: true,
        confirmButtonColor: '#E7A016',
        confirmButtonText: 'Confirmar',
        heightAuto: false,
      }).then(async (result) => {
        if (result.value) {
          this.presentLoading( 'Cargando' );
          (await this.businessService.answerDoubt(data))
          .subscribe( (resp : any) => {
            console.log(resp)
            // this.navCtrl.navigateRoot('/tabs/pendientes');
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          }, (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          });
        }
      })
    }
  }

  goToCuentas(){  
    if ( this.cuentas.length > 0 ){
      this.storage.setCuentas( JSON.stringify( this.cuentas ) ) 
      this.navCtrl.navigateForward( '/tabs/pendientes/datos-deuda' );
    }
  }

  registrarPago(){
    this.storage.setCuentas( JSON.stringify(this.cuentas) );
    this.navCtrl.navigateForward('/tabs/pendientes/registro-pago');
  }

}
