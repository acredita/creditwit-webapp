import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleDeudaPubPage } from './detalle-deuda-pub.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleDeudaPubPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleDeudaPubPageRoutingModule {}
