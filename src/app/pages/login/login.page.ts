import { Component, OnInit, NgModule } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { NavController, LoadingController, Platform } from "@ionic/angular";
import { StorageService } from "src/app/services/storage.service";
import Swal from "sweetalert2";
import { AuthService } from "src/app/services/auth.service";
import { JsonPipe } from "@angular/common";
import { PushService } from "../../services/push.service";
import { BusinessService } from "../../services/business.service";
import { ActivatedRoute } from "@angular/router";
import { ClientesService } from "../../services/clientes.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  idInvalid: boolean = false;
  passInvalid: boolean = false;
  error: boolean = false;
  loading: any;
  keyboard: boolean = true;
  predefinedPath: string = "";
  idDeuda: any;
  fixedID: string;
  hide = false;

  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 6000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
  });

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private authService: AuthService,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private pushService: PushService,
    private platform: Platform,
    private businessService: BusinessService,
    private route: ActivatedRoute,
    private clientesService: ClientesService
  ) {
    if (this.route.snapshot.queryParamMap.get("m")) {
      // ruta publica entonces debe consultar el detalle de la deuda

      switch (this.route.snapshot.queryParamMap.get("m")) {
        case "dD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "rP":
          this.predefinedPath = "/tabs/pendientes/rechazar-pago";
          break;
        case "rC":
          this.predefinedPath = "/tabs/pendientes/rechazo-cobro";
          break;
        case "sCD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "pD":
          this.predefinedPath = "/tabs/pendientes/detalle-pago";
          break;
        default:
          break;
      }

      console.log("path del modulo: " + this.predefinedPath);

      this.idDeuda = this.route.snapshot.queryParamMap.get("d");
      console.log("id de la deuda: " + this.idDeuda);
      const deudaRef = {
        idDeuda: this.idDeuda
      };
      this.storage.setDeudaRef(JSON.stringify(deudaRef));
    }

    if (this.storage.getPath()) {
      let path = JSON.parse(this.storage.getPath());
      switch (path.modulo) {
        case "dD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "rP":
          this.predefinedPath = "/tabs/pendientes/rechazar-pago";
          break;
        case "rC":
          this.predefinedPath = "/tabs/pendientes/rechazo-cobro";
          break;
        case "sCD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "pD":
          this.predefinedPath = "/tabs/pendientes/detalle-pago";
          break;
        default:
          break;
      }

      this.idDeuda = path.id;
      console.log("id de la deuda: " + this.idDeuda);
      const deudaRef = {
        idDeuda: this.idDeuda
      };
      this.storage.setDeudaRef(JSON.stringify(deudaRef));
      this.storage.removePath();
    }

    if (this.platform.is("cordova")) {
      if (this.storage.getToken()) {
        if (this.predefinedPath != "") {
          let path = this.predefinedPath;
          this.predefinedPath = "";
          this.navCtrl.navigateRoot(path);
        } else {
          this.navCtrl.navigateRoot("/tabs/enviar-cobro");
        }
      }
    }
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      letter: ["V", [Validators.required]],
      id: ["", [Validators.required]],
      pass: ["", [Validators.required, Validators.minLength(8)]]
    });
    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  ionViewWillEnter() {
    this.predefinedPath = "";
    this.hide = false;
    if (this.route.snapshot.queryParamMap.get("m")) {
      // ruta publica entonces debe consultar el detalle de la deuda
      switch (this.route.snapshot.queryParamMap.get("m")) {
        case "dD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "rP":
          this.predefinedPath = "/tabs/pendientes/rechazar-pago";
          break;
        case "rC":
          this.predefinedPath = "/tabs/pendientes/rechazo-cobro";
          break;
        case "sCD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          if (!this.platform.is("cordova")) {
            this.Toast.fire({
              showConfirmButton: true,
              confirmButtonText: "Descargar",
              confirmButtonColor: "#E7A016",
              title: "¡Disfruta de Creditwit en tu celular!"
            }).then((result) => {
              if (result.value) {
                if (this.platform.is("ios")) {
                  window.open("https://www.google.com", "_blank");
                } else {
                  window.open("https://www.youtube.com", "_blank");
                }
              }
            });
          }
          break;
        case "pD":
          this.predefinedPath = "/tabs/pendientes/detalle-pago";
          break;
        default:
          break;
      }

      this.idDeuda = this.route.snapshot.queryParamMap.get("d");
      const deudaRef = {
        idDeuda: this.idDeuda
      };

      this.storage.setDeudaRef(JSON.stringify(deudaRef));
    }

    if (this.storage.getPath()) {
      let path = JSON.parse(this.storage.getPath());
      switch (path.modulo) {
        case "dD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "rP":
          this.predefinedPath = "/tabs/pendientes/rechazar-pago";
          break;
        case "rC":
          this.predefinedPath = "/tabs/pendientes/rechazo-cobro";
          break;
        case "sCD":
          this.predefinedPath = "/tabs/pendientes/detalle-deuda";
          break;
        case "pD":
          this.predefinedPath = "/tabs/pendientes/detalle-pago";
          break;
        default:
          break;
      }

      this.idDeuda = path.id;
      console.log("id de la deuda: " + this.idDeuda);
      const deudaRef = {
        idDeuda: this.idDeuda
      };
      this.storage.setDeudaRef(JSON.stringify(deudaRef));
      this.storage.removePath();
    }

    if (this.platform.is("cordova")) {
      if (this.storage.getToken()) {
        if (this.predefinedPath != "") {
          let path = this.predefinedPath;
          this.predefinedPath = "";
          this.navCtrl.navigateRoot(path);
        } else {
          this.navCtrl.navigateRoot("/tabs/enviar-cobro");
        }
      }
    }
  }

  async onLogin() {
    const form = this.loginForm.value;
    let context = {
      isUserLoggedIn: true
    };

    if (form.id == "") {
      this.idInvalid = true;
    }

    if (form.pass == "") {
      this.passInvalid = true;
    }

    if (this.loginForm.valid) {
      this.presentLoading("Validando...");
      let data = {
        user:
          form.letter +
          form.id
            .replace(",", "")
            .replace(",", "")
            .replace(",", "")
            .replace(",", "")
            .replace(".", "")
            .replace(".", "")
            .replace(".", "")
            .replace(".", "")
            .replace(".", ""),
        pass: form.pass
      };
      this.authService.login(data).subscribe(
        (resp: any) => {
          console.log(resp);
          this.storage.setRefreshToken(resp.refreshToken);
          if (resp.isActive) {
            this.storage.setUserData(JSON.stringify(resp.userData));
            this.storage.setToken(resp.accessToken);
            if (
              this.platform.is("cordova") &&
              resp.userData.config.pushNotifications
            ) {
              if (resp.userData.config.playerId) {
                this.pushService.configuracionInicial();
                this.pushService.configuracionInicial();
              } else {
                Swal.fire({
                  text: "¿Desea recibir notificaciones?",
                  showCancelButton: true,
                  confirmButtonColor: "#E7A016",
                  confirmButtonText: "Si",
                  cancelButtonColor: "#E7A016",
                  cancelButtonText: "No",
                  heightAuto: false
                }).then(async (result) => {
                  if (result.value) {
                    this.pushService.configuracionInicial();
                    this.pushService.configuracionInicial();
                  } else {
                    (await this.businessService.activePush()).subscribe(
                      (resp: any) => {
                        console.log(resp);
                        let userData = JSON.parse(this.storage.getUserData());
                        userData.config.pushNotifications =
                          resp.pushNotifications;
                        this.storage.setUserData(JSON.stringify(userData));
                      },
                      (err) => {
                        console.log(err);
                      }
                    );
                  }
                });
              }
            }
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
            this.storage.setContext(JSON.stringify(context));
            if (this.predefinedPath != "") {
              let path = this.predefinedPath;
              this.predefinedPath = "";
              this.navCtrl.navigateRoot(path);
            } else {
              this.navCtrl.navigateRoot("/tabs/enviar-cobro");
            }
          } else {
            let data = {
              phone: resp.userData.telefono.replace("+58", 0)
            };
            this.storage.setRegistro2(JSON.stringify(data));
            this.clientesService.resetConfirmClient().subscribe(
              (resp: any) => {
                this.navCtrl.navigateForward("/registro3");
                setTimeout(() => {
                  this.loading.dismiss();
                }, 500);
              },
              (err) => {
                console.log(err);
                setTimeout(() => {
                  this.loading.dismiss();
                }, 500);
                if (err.error.error == "unAuthorized") {
                  Swal.fire({
                    confirmButtonColor: "#E7A016",
                    text: err.error.error_description,
                    heightAuto: false,
                    showCloseButton: true
                  });
                }
              }
            );
          }
        },
        (err) => {
          console.log(err);
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
          this.loginForm.patchValue({
            pass: ""
          });
          if (err.error.error == "WrongData") {
            Swal.fire({
              confirmButtonColor: "#E7A016",
              text: err.error.error_description,
              heightAuto: false,
              showCloseButton: true
            });
          }
        }
      );
    }
  }

  get errorControl() {
    return this.loginForm.controls;
  }

  onInput(event: any) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  onEnter(event: any) {
    //console.log(event)
    if (event.key === "Enter") {
      this.onLogin();
    }
  }

  formatID(event: any) {
    //console.log(' ID ' + event.target.value);
    if (event.detail.value !== "") {
      let value = String(event.detail.value)
        .replace(".", "")
        .replace(".", "")
        .replace(".", "")
        .replace(",", "")
        .replace(",", "")
        .replace(",", "");
      this.fixedID = parseInt(value)
        .toString()
        .replace(/\./g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    //console.log('fixed ID ' + this.fixedID);
  }

  hidden(){
    this.hide = !this.hide;
  }
}
