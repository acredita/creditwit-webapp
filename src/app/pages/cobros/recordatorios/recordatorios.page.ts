import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-recordatorios',
  templateUrl: './recordatorios.page.html',
  styleUrls: ['./recordatorios.page.scss'],
})
export class RecordatoriosPage implements OnInit {

  loading: any;
  deuda: any;
  currency;
  nombre;
  monto;
  tipo = "1";
  dias;
  mensaje = '';

  constructor( private navCtrl: NavController,
    private storage: StorageService,
    private businessService: BusinessService,
    private loadingCtrl: LoadingController, ) { }

  ngOnInit() {
    this.deuda = JSON.parse( this.storage.getRecordatorio() );
  }

  ionViewWillEnter() {
    this.deuda = JSON.parse( this.storage.getRecordatorio() );
    console.log( this.deuda );
    this.nombre = this.deuda.deudor.nombres + " " + this.deuda.deudor.apellidos;
    if ( this.deuda.currency == 'usd' ){
      this.currency = '$';
    } else{
      this.currency = 'Bs.';
    }
    this.monto = (this.deuda.monto/100).toLocaleString("de-DE", { maximumFractionDigits: 2, minimumFractionDigits: 2 });
  }

  onInput(event: any) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  async registerDoubt() {

    if ( this.tipo == '2' && this.mensaje == ''){
      Swal.fire({
        confirmButtonColor: "#E7A016",
        text: 'El mensaje es requerido',
        heightAuto: false,
        showCloseButton: true
      });
      return
    }

    let data = this.deuda;

    if ( Number( this.dias ) <= 0 || this.dias == '' || this.dias == undefined ){
      this.dias = 1;
    }

    data.frecuencia = Number(this.dias);

    if ( this.tipo == '1' ){
      data.sms = '';
    } else if ( this.tipo == '2' ){
      data.sms = this.mensaje;
    } else{
      data.sms = 'no';
    }
    console.log(data)

    this.presentLoading("Cargando...");
    (await this.businessService.registrarDeuda(data)).subscribe(
      (resp: any) => {
        console.log("resp: " + JSON.stringify(resp));
        this.storage.setDeuda(JSON.stringify(data));
        setTimeout(() => {
          this.loading.dismiss();
          this.navCtrl.navigateForward("/tabs/enviar-cobro/success");
        }, 1000);
      },
      (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000);
        if (err.error.error) {
          Swal.fire({
            confirmButtonColor: "#E7A016",
            text: err.error.error_description,
            heightAuto: false,
            showCloseButton: true
          });
          this.navCtrl.navigateRoot("/tabs/enviar-cobro");
        }
      }
    );
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  goBack() {
    this.navCtrl.navigateBack("/tabs/enviar-cobro/resume");
  }

}
