import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeudaRegistradaPage } from './deuda-registrada.page';

describe('DeudaRegistradaPage', () => {
  let component: DeudaRegistradaPage;
  let fixture: ComponentFixture<DeudaRegistradaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeudaRegistradaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeudaRegistradaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
