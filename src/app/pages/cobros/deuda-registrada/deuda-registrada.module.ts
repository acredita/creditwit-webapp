import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeudaRegistradaPageRoutingModule } from './deuda-registrada-routing.module';

import { DeudaRegistradaPage } from './deuda-registrada.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeudaRegistradaPageRoutingModule
  ],
  declarations: [DeudaRegistradaPage]
})
export class DeudaRegistradaPageModule {}
