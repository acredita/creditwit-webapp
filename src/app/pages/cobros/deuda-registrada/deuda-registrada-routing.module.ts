import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeudaRegistradaPage } from './deuda-registrada.page';

const routes: Routes = [
  {
    path: '',
    component: DeudaRegistradaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeudaRegistradaPageRoutingModule {}
