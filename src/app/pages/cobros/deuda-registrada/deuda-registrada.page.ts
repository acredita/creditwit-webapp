import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-deuda-registrada',
  templateUrl: './deuda-registrada.page.html',
  styleUrls: ['./deuda-registrada.page.scss'],
})
export class DeudaRegistradaPage implements OnInit {

  nombre: any;
  cedula: '';
  monto: any;
  currency: any;
  fechaLimite: string = '';
  dias: '';
  concepto: '';
  deudaData: any;

  constructor(
    private navCtrl: NavController,
    private storage: StorageService,
  ) { }

  ngOnInit() {

    this.deudaData = JSON.parse(this.storage.getDeuda());
    console.log("info deuda: "  + JSON.stringify(this.deudaData));
    this.nombre = this.deudaData.deudor.nombres + ' ' + this.deudaData.deudor.apellidos;
    this.cedula = this.deudaData.deudor.cedula;
    this.monto = (parseFloat(this.deudaData.monto) / 100).toLocaleString('de-DE');
    this.currency = this.deudaData.currency == 'bs' ? 'Bs' : '$';
    this.monto = this.currency  + ' ' + this.monto;
    this.fechaLimite = formatDate(this.deudaData.fechaLimite, 'dd/MM/yyyy', 'en');
    this.concepto = this.deudaData.concepto;
  }

  onAccept(){
    this.navCtrl.navigateRoot('tabs/enviar-cobro');
  }

}
