import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CobroResumePage } from './cobro-resume.page';

const routes: Routes = [
  {
    path: '',
    component: CobroResumePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CobroResumePageRoutingModule {}
