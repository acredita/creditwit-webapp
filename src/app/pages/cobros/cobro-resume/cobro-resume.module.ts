import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CobroResumePageRoutingModule } from './cobro-resume-routing.module';
import { BrMaskerModule } from 'br-mask';
import { CobroResumePage } from './cobro-resume.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CobroResumePageRoutingModule,
    BrMaskerModule
  ],
  declarations: [CobroResumePage]
})
export class CobroResumePageModule {}
