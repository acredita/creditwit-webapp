import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CobroResumePage } from './cobro-resume.page';

describe('CobroResumePage', () => {
  let component: CobroResumePage;
  let fixture: ComponentFixture<CobroResumePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobroResumePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CobroResumePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
