import { BusinessService } from "src/app/services/business.service";
import { NavController, LoadingController } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { StorageService } from "src/app/services/storage.service";
import { formatDate } from "@angular/common";
import Swal from "sweetalert2";

@Component({
  selector: "app-cobro-resume",
  templateUrl: "./cobro-resume.page.html",
  styleUrls: ["./cobro-resume.page.scss"]
})
export class CobroResumePage implements OnInit {
  chargeForm: FormGroup;
  loading: any;
  keyboard: boolean = true;
  registrado: boolean = false;
  hasPhone: boolean = false;
  monto: any;
  currency: any;
  deudaData: any;
  cedula: "";
  puntuacion: any;
  calificaciones: any;
  nombre: any;
  telefonoInvalid: boolean = false;
  nombresInvalid: boolean = false;
  apellidosInvalid: boolean = false;
  plazoInvalid: boolean = false;
  fechaLimite: string = "Fecha límite";
  formatedDate: string = "";
  dateInvalid: boolean = false;
  cedulaAux;

  constructor(
    private navCtrl: NavController,
    public fb: FormBuilder,
    private storage: StorageService,
    private businessService: BusinessService,
    private loadingCtrl: LoadingController
  ) {}

  ngOnInit() {
    this.chargeForm = this.fb.group({
      plazo: ["", [Validators.required]],
      nombres: ["", [Validators.required]],
      apellidos: ["", [Validators.required]],
      code: ["414"],
      telefono: ["", [Validators.required]],
      concepto: [""]
    });
    this.deudaData = JSON.parse(this.storage.getDeuda());
    console.log(this.deudaData);
    this.cedula = this.deudaData.cedula;
    this.cedulaAux = `${this.deudaData.cedula.substring(0, 1)} ${Number(
      this.deudaData.cedula.substring(1, this.deudaData.cedula.length)
    ).toLocaleString("de-DE")}`;

    if (this.deudaData.registrado) {
      // Si el usuario esta registrado se llenan los campos nombres,apellido,telf y se muestra esta informacion.
      this.chargeForm.patchValue({
        nombres: this.deudaData.nombres,
        apellidos: this.deudaData.apellidos,
        telefono: this.deudaData.telefono
      });

      this.registrado = true;

      this.hasPhone = this.deudaData.hasPhone;

      this.nombre = this.deudaData.nombres + " " + this.deudaData.apellidos;
    } else {
      // si no esta registrado debemos recibir la data en el form
      this.registrado = false;
      if (this.deudaData.nombres) {
        this.chargeForm.patchValue({
          nombres: this.deudaData.nombres,
          apellidos: this.deudaData.apellidos,
          telefono: this.deudaData.telefono.toString().replace("58", "0")
        });
      }
    }
    if (!this.deudaData.calificaciones || !this.deudaData.puntuacion) {
      this.puntuacion = "NA";
      this.calificaciones = "";
    } else {
      this.puntuacion = this.deudaData.puntuacion;
      this.calificaciones = "(" + this.deudaData.calificaciones + ")";
    }

    this.monto = this.deudaData.monto;
    this.currency = this.deudaData.currency;

    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  onInput(event: any) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  capitalizeString(string) {
    return string
      .split(" ")
      .map(
        (word) => word.substr(0, 1).toUpperCase() + word.substr(1, word.length)
      )
      .join(" ");
  }

  async onSubmit() {
    const form = this.chargeForm.value;
    let phone = form.telefono;
    let plazo = form.plazo;
    this.nombresInvalid = false;
    this.apellidosInvalid = false;
    this.telefonoInvalid = false;
    this.plazoInvalid = false;
    if (!this.hasPhone) {
      phone = `+58${form.code}${form.telefono}`;
      console.log("telefono: " + phone);
    }
    console.log("form: " + JSON.stringify(form));
    let deudorInfo = {
      // informacion referente al deudor
      nombres: this.capitalizeString(form.nombres.toString().toLowerCase()),
      apellidos: this.capitalizeString(form.apellidos.toString().toLowerCase()),
      registrado: this.registrado,
      cedula: this.cedula,
      telefono: phone
    };

    console.log("Data del deudor: " + JSON.stringify(deudorInfo));
    if (!this.registrado) {
      console.log("deudor no registrado");
      if (form.nombres == "") {
        this.nombresInvalid = true;
      }
      if (form.apellidos == "") {
        this.apellidosInvalid = true;
      }
      if (form.telefono == "" || form.telefono.length < 7) {
        this.telefonoInvalid = true;
      }
    }
    if (plazo == "") {
      this.plazoInvalid = true;
    }
    // MANEJO DEL MONTO
    let amountForAPI: any = this.monto;
    let tipoMoneda;
    if (this.currency.toString().toLowerCase() == "bs") {
      amountForAPI = amountForAPI.replace(/\./g, "").replace(",", ".");
      tipoMoneda = "bs";
    } else {
      tipoMoneda = "usd";
      amountForAPI = amountForAPI.replace(/\./g, "").replace(",", ".");
    }
    console.log("amount for API: " + amountForAPI);
    
    amountForAPI = Math.round( parseFloat(amountForAPI) * 100 );
    console.log("amount for API parsed: " + amountForAPI);

    console.log("fecha limite: " + new Date(this.fechaLimite));
    if (
      this.chargeForm.valid &&
      !this.nombresInvalid &&
      !this.nombresInvalid &&
      !this.apellidosInvalid &&
      !this.plazoInvalid &&
      !this.dateInvalid &&
      !this.telefonoInvalid
    ) {
      const dataForAPI = {
        monto: amountForAPI,
        currency: tipoMoneda,
        fechaLimite: this.formatedDate,
        concepto: form.concepto,
        deudor: deudorInfo
      };
      this.storage.setRecordatorio(JSON.stringify(dataForAPI));
      this.navCtrl.navigateForward('/tabs/enviar-cobro/recordatorios');
    } else {
      console.log("form invalid");
    }
  }

  async registerDoubt(data: any) {
    console.log("Data: " + JSON.stringify(data));

    this.presentLoading("Cargando...");
    (await this.businessService.registrarDeuda(data)).subscribe(
      (resp: any) => {
        console.log("resp: " + JSON.stringify(resp));
        this.storage.setDeuda(JSON.stringify(data));
        setTimeout(() => {
          this.loading.dismiss();
          this.navCtrl.navigateForward("/tabs/enviar-cobro/success");
        }, 1000);
      },
      (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 1000);
        if (err.error.error) {
          Swal.fire({
            confirmButtonColor: "#E7A016",
            text: err.error.error_description,
            heightAuto: false,
            showCloseButton: true
          });
          this.navCtrl.navigateBack("/tabs/enviar-cobro/");
        }
      }
    );
  }

  getDeadline() {
    this.plazoInvalid = false;
    this.dateInvalid = false;
    const form = this.chargeForm.value;
    let plazo = form.plazo;
    if (plazo == "" || plazo < 0) {
      this.plazoInvalid = true;
    } else if (plazo > 365) {
      this.dateInvalid = true;
    } else {
      let fechaActual = new Date();
      let deadline = new Date();
      deadline.setTime(fechaActual.getTime() + 1000 * 60 * 60 * 24 * plazo);
      this.fechaLimite = formatDate(deadline, "dd/MM/yyyy", "en");
      this.formatedDate = formatDate(deadline, "yyyy/MM/dd", "en");
    }
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  goBack() {
    this.navCtrl.navigateBack("/tabs/enviar-cobro");
  }

  get errorControl() {
    return this.chargeForm.controls;
  }
}
