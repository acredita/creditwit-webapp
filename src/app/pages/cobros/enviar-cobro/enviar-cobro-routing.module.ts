import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnviarCobroPage } from './enviar-cobro.page';

const routes: Routes = [
  {
    path: '',
    component: EnviarCobroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnviarCobroPageRoutingModule {}
