import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnviarCobroPage } from './enviar-cobro.page';

describe('EnviarCobroPage', () => {
  let component: EnviarCobroPage;
  let fixture: ComponentFixture<EnviarCobroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnviarCobroPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnviarCobroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
