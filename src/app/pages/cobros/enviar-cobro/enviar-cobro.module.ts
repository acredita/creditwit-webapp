import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnviarCobroPageRoutingModule } from './enviar-cobro-routing.module';

import { EnviarCobroPage } from './enviar-cobro.page';
import { DecimalPipe } from '@angular/common';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EnviarCobroPageRoutingModule,
    ComponentsModule
  ],
  declarations: [EnviarCobroPage]
})
export class EnviarCobroPageModule {}
