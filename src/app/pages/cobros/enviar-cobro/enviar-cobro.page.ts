import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from "@angular/forms";
import { NavController, LoadingController } from "@ionic/angular";
import { StorageService } from "src/app/services/storage.service";
import { BusinessService } from "src/app/services/business.service";
import { DecimalPipe } from "@angular/common";
import Swal from "sweetalert2";

@Component({
  selector: "app-enviar-cobro",
  templateUrl: "./enviar-cobro.page.html",
  styleUrls: ["./enviar-cobro.page.scss"]
})
export class EnviarCobroPage implements OnInit {
  newChargeForm: FormGroup;
  id: string = "";
  amount: string = "";
  idInvalid: boolean = false;
  idRepeated: boolean = false;
  emptyID: boolean = false;
  amountInvalid: boolean = false;
  error: boolean = false;
  loading: any;
  fixedAmount: any;
  monto: any;
  fixedID: any;
  keyboard: boolean = true;
  preventDefault: boolean = false;
  predefinedIDType = "V";
  predefinedCurrencyType = "$";
  directorio: boolean = false;
  usuario: any;
  amountCurrency;

  precision = 2;

  entry;

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private businessService: BusinessService,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private decimalPipe: DecimalPipe
  ) {}

  ngOnInit() {
    this.newChargeForm = this.fb.group({
      letter: ["V", [Validators.required]],
      id: ["", [Validators.required]],
      currencyType: ["$", [Validators.required]],
      amount: ["", [Validators.required]]
    });
    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  ionViewWillEnter() {
    this.amountCurrency = '';
    let data = {
      enviarCobro: false
    };
    this.storage.setContext(JSON.stringify(data));
    this.newChargeForm.patchValue({
      letter: "V",
      currencyType: "$",
      amount: ''
    });
    this.usuario = JSON.parse( this.storage.getUserDirectorio() );
    if ( this.usuario ){
      this.fixedID = this.usuario.cedula.substring(1, this.usuario.cedula.length);
      this.newChargeForm.patchValue({
        letter: this.usuario.letter,
        currencyType: "$"
      });
      this.storage.removeUserDirectorio();
    } else{
      this.directorio = false;
      this.getDirectorio();
    }
  }

  get errorControl() {
    return this.newChargeForm.controls;
  }

  cleanInputError() {
    this.idInvalid = false;
    this.amountInvalid = false;
    this.idRepeated = false;
    this.emptyID = false;
  }

  checkUserAccountsNumber() {
    let userData = JSON.parse(this.storage.getUserData());
    let numCuentas = userData.payMethods.length;
    console.log(userData);
    if (numCuentas == 0) {
      // muestra aviso y llama a endpoint con respuesta negativa
      Swal.fire({
        text:
          "Debes registrar un método de pago \n antes de registrar un cobro",
        showCancelButton: false,
        showCloseButton: true,
        confirmButtonColor: "#E7A016",
        confirmButtonText: "Registrar",
        heightAuto: false
      }).then((result) => {
        if (result.value) {
          let data = {
            enviarCobro: true
          };
          this.storage.setContext(JSON.stringify(data));
          this.navCtrl.navigateForward("/tabs/perfil/agregar-datos");
        }
      });
    } else {
      this.newCharge();
    }
  }

  async newCharge() {
    // ENDPOINT
    const form = this.newChargeForm.value;

    if (form.id === "" || form.id == undefined) {
      this.emptyID = true;
    } else {
      if (form.id.toString().replace(/\./g, "").replace(/,/g, "").length < 6) {
        this.idInvalid = true;
      }

      if (
        form.letter + form.id.toString().replace(/\./g, "").replace(/,/g, "") ==
        JSON.parse(this.storage.getUserData()).cedula
      ) {
        this.idRepeated = true;
      }
    }
    if (form.amount == "" || form.amount == "0" || form.amount == undefined) {
      this.amountInvalid = true;
    } else{
      this.amountInvalid = false;
    }

    if (!this.idInvalid && !this.idRepeated && !this.amountInvalid) {
      console.log(form.amount);
      let data = {
        // obj que se recibe de back que hace referencia al deudor
        cedula:
          form.letter + form.id.toString().replace(/\./g, "").replace(/,/g, ""), // concat tipo de ID + numero de identificacion
        telefono: "",
        nombres: "",
        apellidos: "",
        registrado: false,
        puntuacion: "",
        monto: form.amount,
        currency: form.currencyType,
        calificaciones: "",
        hasPhone: false
      };
      if ( this.usuario ){
        data.nombres = this.usuario.nombre;
        data.apellidos = this.usuario.apellido;
        data.puntuacion = this.usuario.rating.puntuacion;
        data.calificaciones = this.usuario.rating.numeroCalificaciones;
        data.registrado = true;
        data.cedula = this.usuario.cedula;
        data.hasPhone = true;
        data.telefono = this.usuario.telefono;
        this.storage.setDeuda(JSON.stringify(data));
        this.cleanInputError();
        this.newChargeForm.reset();
        this.navCtrl.navigateForward("/tabs/enviar-cobro/resume");
      } else{
        console.log(JSON.stringify(data));
        this.presentLoading("Cargando...");
        (await this.businessService.getDeudor(data.cedula)).subscribe(
          (resp: any) => {
            console.log("resp: " + JSON.stringify(resp));
            data.nombres = resp.nombres;
            data.apellidos = resp.apellidos;
            data.telefono = resp.telefono;
            data.registrado = resp.registrado;
            if (data.telefono !== "") {
              data.hasPhone = true;
            }
            data.puntuacion = resp.rating.puntuacion;
            data.calificaciones = resp.rating.numeroCalificaciones;
            this.storage.setDeuda(JSON.stringify(data));
            setTimeout(() => {
              this.loading.dismiss();
              this.clearAmount();
              this.fixedAmount = "";
              this.fixedID = "";
              this.cleanInputError();
              this.newChargeForm.reset();
              this.navCtrl.navigateForward("/tabs/enviar-cobro/resume");
            }, 1000);
          },
          (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 1000);
            if (err.error.error) {
              Swal.fire({
                confirmButtonColor: "#E7A016",
                text: err.error.error_description,
                heightAuto: false,
                showCloseButton: true
              });
            }
          }
        );
      }
    } else {
      console.log("Registro deuda invalido");
    }
  }

  onInput(event: any) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  onChange(event: any) {
    this.preventDefault = false;
    //console.log(event)
    let value: string = event.target.value;
    //console.log(value);
    if (value !== "") {
      if (this.newChargeForm.value.currencyType.toLowerCase() == "bs") {
        // FORMATO EN BOLIVARES
        if (value.includes(",")) {
          const regex = /[0-9]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        } else {
          const regex = /[0-9,","]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        }
      } else {
        // FORMATO EN DOLARES
        console.log("escribiendo en usd");
        if (value.includes(".")) {
          let regex = /[0-9]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        } else {
          let regex = /[0-9 "."]/;
          if (!regex.test(event.key)) {
            if (event.key !== "Backspace") {
              event.preventDefault();
              this.preventDefault = true;
            }
          }
        }
      }
    }
  }

  formatID(event: any) {
    this.cleanInputError();
    //console.log(' ID ' + event.target.value);
    if (event.detail.value !== "") {
      let value = String(event.detail.value)
        .replace(".", "")
        .replace(".", "")
        .replace(".", "")
        .replace(",", "")
        .replace(",", "")
        .replace(",", "");
      this.fixedID = parseInt(value)
        .toString()
        .replace(/\./g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    //console.log('fixed ID ' + this.fixedID);
  }

  afterTextChanged(event: any) {
    this.cleanInputError();
    let value = event.target.value;
    if (value != "") {
      if (this.newChargeForm.value.currencyType.toLowerCase() == "bs") {
        // FORMATO EN BOLIVARES
        this.fixedAmount = value
          .toString()
          .replace(/\./g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      } else {
        // FORMATO EN DOLARES
        this.fixedAmount = value
          .toString()
          .replace(/,/g, "")
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    } else {
      this.fixedAmount = "";
    }

    this.newChargeForm.patchValue({ amount: this.fixedAmount });
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  clearAmount() {
    const form = this.newChargeForm.value;
    if (form.amount != 0 && form.amount != "" && form.amount != undefined) {
      if (form.currencyType == "Bs") {
        this.fixedAmount = form.amount
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "")
          .replace(",", "");
        if (this.fixedAmount.toString().includes(".")) {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("de-DE", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          });
        } else {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("de-DE");
        }
      } else {
        this.fixedAmount = form.amount
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(".", "")
          .replace(",", ".");
        if (this.fixedAmount.toString().includes(".")) {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("en", {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          });
        } else {
          this.fixedAmount = Number(this.fixedAmount).toLocaleString("en");
        }
      }
    }
    this.newChargeForm.patchValue({ amount: this.fixedAmount });
    this.amountInvalid = false;
    this.idInvalid = false;
  }

  onEnter(event: any) {
    console.log(event);
    if (event.key === "Enter") {
      this.newCharge();
    }
  }

  async getDirectorio(){
    try {
      const resp:any = await this.businessService.getDirectory();
      console.log(resp)
      if ( resp.length > 0 ){
        this.directorio = true;
        this.storage.setDirectorio( JSON.stringify( resp ) );
      }
    } catch (error) {
      console.log(error);
    }
  }

  goToDirectorio(){
    this.navCtrl.navigateForward('/tabs/enviar-cobro/directorio');
  }

  amountChanged(event: number) {
    this.newChargeForm.patchValue({
      amount: (event / Math.pow(10, this.precision)).toLocaleString("de-DE"),
    });
    this.amountCurrency = event;
  }

}
