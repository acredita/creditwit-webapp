import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-directorio',
  templateUrl: './directorio.page.html',
  styleUrls: ['./directorio.page.scss'],
})
export class DirectorioPage implements OnInit {

  directorios = [];
  directorioAux = [];
  buscar = '';

  constructor( private navCtrl: NavController,
    private storage: StorageService ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.directorios = JSON.parse( this.storage.getDirectorio() );
    console.log(this.directorios)
    this.directorios.forEach(usuario => {
      usuario.letter = usuario.cedula.substring(0,1);
      usuario.number = Number(usuario.cedula.substring(1, usuario.cedula.length)).toLocaleString("de-DE");
      usuario.buscar = usuario.nombre + ' ' + usuario.apellido;
    });
    this.directorioAux = this.directorios;
    this.buscar = '';
  }

  goBack() {
    this.navCtrl.navigateBack("/tabs/enviar-cobro");
  }

  setUsuario( usuario ){
    this.storage.setUserDirectorio( JSON.stringify( usuario ) );
    this.goBack();
  }

  filterArray(){
    this.directorios = this.directorioAux.filter(usuario => String(usuario.buscar).toLowerCase().includes( this.buscar.toLowerCase() ));
  }

}
