import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  credito = "./assets/images/icons/credito.svg";
  perfilActive = "./assets/images/icons/perfil.svg";
  isCreditoSelected = false;
  isSearchSelected= false;

  constructor() { }

  ngOnInit() {
  }

  resetAll(){
    this.isCreditoSelected = false;
    this.isSearchSelected= false;
  }

  changeCreditoIcon() {    
    this.resetAll();
    this.isCreditoSelected = true;
  }

}
