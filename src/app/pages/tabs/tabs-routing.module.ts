import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'pendientes'
  },
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'perfil',
        children: [
          {
            path: '',
            loadChildren: () => import('./../mi-perfil/perfil/perfil.module').then( m => m.PerfilPageModule)
          },
          {
            path: 'datos-bancarios',
            loadChildren: () => import('./../mi-perfil/datos-bancarios/datos-bancarios.module').then( m => m.DatosBancariosPageModule)
          },
          {
            path: 'agregar-datos',
            loadChildren: () => import('./../mi-perfil/agregar-datos/agregar-datos.module').then( m => m.AgregarDatosPageModule)
          },
          {
            path: 'cuenta-bancaria',
            loadChildren: () => import('./../mi-perfil/cuenta-bancaria/cuenta-bancaria.module').then( m => m.CuentaBancariaPageModule)
          },
          {
            path: 'zelle',
            loadChildren: () => import('./../mi-perfil/zelle/zelle.module').then( m => m.ZellePageModule)
          },
          {
            path: 'pago-movil',
            loadChildren: () => import('./../mi-perfil/pago-movil/pago-movil.module').then( m => m.PagoMovilPageModule)
          },
          {
            path: 'otros',
            loadChildren: () => import('./../mi-perfil/otros/otros.module').then( m => m.OtrosPageModule)
          },
          {
            path: 'exito',
            loadChildren: () => import('./../mi-perfil/exito/exito.module').then( m => m.ExitoPageModule)
          },
          {
            path: 'historial-evaluaciones',
            loadChildren: () => import('./../mi-perfil/historial-evaluaciones/historial-evaluaciones.module').then( m => m.HistorialEvaluacionesPageModule)
          },
        
        ]
      },
      {
        path: 'enviar-cobro',
        children: [
          {
            path: '',
            loadChildren: () => import('./../cobros/enviar-cobro/enviar-cobro.module').then(m => m.EnviarCobroPageModule)
          },
          {
            path: 'directorio',
            loadChildren: () => import('./../cobros/directorio/directorio.module').then( m => m.DirectorioPageModule)
          },
          {
            path: 'resume',
            loadChildren: () => import('./../cobros/cobro-resume/cobro-resume.module').then( m => m.CobroResumePageModule)
          },
          {
            path: 'success',
            loadChildren: () => import('./../cobros/deuda-registrada/deuda-registrada.module').then( m => m.DeudaRegistradaPageModule)
          },
          {
            path: 'recordatorios',
            loadChildren: () => import('./../cobros/recordatorios/recordatorios.module').then( m => m.RecordatoriosPageModule)
          },
        ]
      },
      {
        path: 'pendientes',
        children: [
          {
            path: '',
            loadChildren: () => import('./../../pages/pendientes/pendientes/pendientes.module').then( m => m.PendientesPageModule)
          },
          {
            path: 'detalle-deuda',
            loadChildren: () => import('./../../pages/pendientes/deudor/detalle-deuda/detalle-deuda.module').then( m => m.DetalleDeudaPageModule)
          },
          {
            path: 'datos-deuda',
            loadChildren: () => import('./../../pages/pendientes/deudor/datos-deuda/datos-deuda.module').then( m => m.DatosDeudaPageModule)
          },
          {
            path: 'detalle-cobro',
            loadChildren: () => import('./../../pages/pendientes/acreedor/detalle-cobro/detalle-cobro.module').then( m => m.DetalleCobroPageModule)
          },
          {
            path: 'registro-pago',
            loadChildren: () => import('./../../pages/pendientes/deudor/registro-pago/registro-pago.module').then( m => m.RegistroPagoPageModule)
          },
          {
            path: 'confirmar-pago',
            loadChildren: () => import('./../../pages/pendientes/deudor/confirmar-pago/confirmar-pago.module').then( m => m.ConfirmarPagoPageModule)
          },
          {
            path: 'rechazar-pago',
            loadChildren: () => import('./../../pages/pendientes/deudor/rechazar-pago/rechazar-pago.module').then( m => m.RechazarPagoPageModule)
          },
          {
            path: 'detalle-pago',
            loadChildren: () => import('./../../pages/pendientes/acreedor/detalle-pago/detalle-pago.module').then( m => m.DetallePagoPageModule)
          },
          {
            path: 'rechazo-cobro',
            loadChildren: () => import('./../../pages/pendientes/acreedor/rechazo-cobro/rechazo-cobro.module').then( m => m.RechazoCobroPageModule)
          },
          {
            path: 'califica-deudor',
            loadChildren: () => import('./../../pages/pendientes/acreedor/califica-deudor/califica-deudor.module').then( m => m.CalificaDeudorPageModule)
          },
          {
            path: 'cobros-por-persona',
            loadChildren: () => import('./../../pages/pendientes/acreedor/cobros-por-persona/cobros-por-persona.module').then( m => m.CobrosPorPersonaPageModule)
          },
          {
            path: 'deudas-por-persona',
            loadChildren: () => import('./../../pages/pendientes/deudor/deudas-por-persona/deudas-por-persona.module').then( m => m.DeudasPorPersonaPageModule)
          },
        ]
      },
      {
        path: 'historial',
        children: [
          {
            path: '',
            loadChildren: () => import('./../../pages/historial/historial/historial.module').then( m => m.HistorialPageModule)
          },
          {
            path: 'detalle-cobro',
            loadChildren: () => import('./../../pages/historial/detalle-cobro/detalle-cobro.module').then( m => m.DetalleCobroPageModule)
          },
          {
            path: 'detalle-deuda',
            loadChildren: () => import('./../../pages/historial/detalle-deuda/detalle-deuda.module').then( m => m.DetalleDeudaPageModule)
          },
        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
