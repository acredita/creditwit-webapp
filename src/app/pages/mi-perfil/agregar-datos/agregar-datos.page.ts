import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { BusinessService } from 'src/app/services/business.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-datos',
  templateUrl: './agregar-datos.page.html',
  styleUrls: ['./agregar-datos.page.scss'],
})
export class AgregarDatosPage implements OnInit {

  loading: HTMLIonLoadingElement;

  constructor( private navCtrl: NavController,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService ) { }

  ngOnInit() {
  }

  goBack(){
    this.navCtrl.navigateBack('/tabs/perfil/datos-bancarios');
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  async addEfectivo(){
    this.presentLoading( 'Validando...' );
    let cuentas = JSON.parse( this.storage.getCuentas());
    let data = {
      currency: 'usd',
      metodo: 'Efectivo',
    };
    let repetido = false;
    cuentas.forEach(cuenta => {
      if ( cuenta.metodo == 'Efectivo' ){
        Swal.fire({
          confirmButtonColor: '#E7A016',
          text: 'Este método de pago ya se encuentra registrado',
          heightAuto: false,
          showCloseButton: true,
        });
        repetido = true;
        return
      }
    });
    if ( !repetido ){
      (await this.businessService.addcuentasBancarias(data))
      .subscribe( (resp : any) => {
        console.log(resp)
        let userData = JSON.parse(this.storage.getUserData());
        userData.payMethods = resp.payMethods;
        this.storage.setUserData( JSON.stringify( userData ) );
        let cuenta = {
          titulo: 'Efectivo agregado',
          line1: 'Recuerda coordinar con el deudor para recibir el pago',
        }
        this.storage.setCuentaExito( JSON.stringify( cuenta ) );
        this.navCtrl.navigateBack('/tabs/perfil/exito');
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
      }, (err) => {
        console.log(err);
        setTimeout(() => {
          this.loading.dismiss();
        }, 500 );
        this.goBack();
      });
    }
    else{
      setTimeout(() => {
        this.loading.dismiss();
      }, 500 );
    }
  }

}
