import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarDatosPage } from './agregar-datos.page';

const routes: Routes = [
  {
    path: '',
    component: AgregarDatosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgregarDatosPageRoutingModule {}
