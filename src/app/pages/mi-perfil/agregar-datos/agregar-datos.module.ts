import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarDatosPageRoutingModule } from './agregar-datos-routing.module';

import { AgregarDatosPage } from './agregar-datos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarDatosPageRoutingModule
  ],
  declarations: [AgregarDatosPage]
})
export class AgregarDatosPageModule {}
