import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistorialEvaluacionesPage } from './historial-evaluaciones.page';

const routes: Routes = [
  {
    path: '',
    component: HistorialEvaluacionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistorialEvaluacionesPageRoutingModule {}
