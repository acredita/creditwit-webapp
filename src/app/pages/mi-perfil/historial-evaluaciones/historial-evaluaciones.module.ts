import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistorialEvaluacionesPageRoutingModule } from './historial-evaluaciones-routing.module';

import { HistorialEvaluacionesPage } from './historial-evaluaciones.page';
import { IonicRatingComponentModule } from 'ionic-rating-component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicRatingComponentModule,
    HistorialEvaluacionesPageRoutingModule
  ],
  declarations: [HistorialEvaluacionesPage]
})
export class HistorialEvaluacionesPageModule {}
