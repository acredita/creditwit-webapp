import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { BusinessService } from 'src/app/services/business.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-historial-evaluaciones',
  templateUrl: './historial-evaluaciones.page.html',
  styleUrls: ['./historial-evaluaciones.page.scss'],
})
export class HistorialEvaluacionesPage implements OnInit {

  /**Data MI PERFIL:
   * rating:
calificaciones: Array(20)
0: {rating: 5, deuda: "5f996090ae8e8c3304bd218b", fecha: "2020-10-30T12:37:58.801Z", rater: {…}}
1: {rating: 5, deuda: "5f9ab623c8e9480b08c7f600", fecha: "2020-10-30T11:22:43.536Z", rater: {…}}
2: {rating: 4, deuda: "5f8885378bc14c37af634030", fecha: "2020-10-30T11:22:14.300Z", rater: {…}}
3: {rating: 2, deuda: "5f8885378bc14c37af634030", fecha: "2020-10-29T20:11:07.445Z", rater: {…}}
4: {rating: 4, deuda: "5f9ab5edc8e9480b08c7f5ff", fecha: "2020-10-29T20:10:11.230Z", rater: {…}}
5: {rating: 4, deuda: "5f996062ae8e8c3304bd2189", fecha: "2020-10-29T20:08:11.650Z", rater: {…}}
6: {rating: 4, deuda: "5f8c722d0e79760985cc1d2e", fecha: "2020-10-29T12:23:23.235Z", rater: {…}}
7: {rating: 5, deuda: "5f9366997c834846eb3d0d20", fecha: "2020-10-28T03:59:35.131Z", rater: {…}}
8: {rating: 5, deuda: "5f9366997c834846eb3d0d20", fecha: "2020-10-28T03:57:59.304Z", rater: {…}}
9: {rating: 3, deuda: "5f7f58b32a73f12ff78bd98c", fecha: "2020-10-25T19:39:49.115Z", rater: {…}}
10: {rating: 4, deuda: "5f8f3c9761b59637b839068e", fecha: "2020-10-20T19:47:22.129Z", rater: {…}}
11: {rating: 3, deuda: "5f8d92a03cff631a478010e2", fecha: "2020-10-19T13:31:45.994Z", rater: {…}}
12: {rating: 5, deuda: "5f8cbb520718e51172dbfe90", fecha: "2020-10-18T22:07:01.285Z", rater: {…}}
13: {rating: 5, deuda: "5f8c563d4a9faa7fc35545a6", fecha: "2020-10-18T14:54:50.095Z", rater: {…}}
14: {rating: null, deuda: "5f7e340d28396f045de7eab5", fecha: "2020-10-12T16:07:46.759Z", rater: {…}}
15: {rating: 3, deuda: "5f7e7fded3054d045b98467d", fecha: "2020-10-09T21:41:36.334Z", rater: {…}}
16: {rating: 3, deuda: "5f7dd569d3054d045b984670", fecha: "2020-10-09T04:01:30.258Z", rater: {…}}
17: {rating: 5, deuda: "5f7e340cd3054d045b984678", fecha: "2020-10-08T19:52:07.179Z", rater: {…}}
18: {rating: 2, deuda: "5f7e285fd3054d045b984672", fecha: "2020-10-08T19:51:57.861Z", rater: {…}}
19: {rating: 4
   */

   /*Data Rating EP: 
   
   */

  loading: any;
  ratings: any;

  constructor(private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController) { }


  async ngOnInit() {
   
    (await this.businessService.getRatingHistory())
      .subscribe((resp: any) => {
        this.ratings = resp;
        console.log(resp);
        setTimeout(() => {
    
        }, 500);
      }, (err) => {
        console.log(err);
        setTimeout(() => {
     
        }, 500);
      });
  }

  ionViewWillEnter(){
      this.ratings = JSON.parse(this.storage.getRatingHistory());
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

goBack(){
  this.navCtrl.navigateBack('/tabs/perfil');
}

}
