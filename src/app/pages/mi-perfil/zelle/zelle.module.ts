import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZellePageRoutingModule } from './zelle-routing.module';

import { ZellePage } from './zelle.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ZellePageRoutingModule,
    ReactiveFormsModule,
    BrMaskerModule
  ],
  declarations: [ZellePage]
})
export class ZellePageModule {}
