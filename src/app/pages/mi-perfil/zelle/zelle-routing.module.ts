import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZellePage } from './zelle.page';

const routes: Routes = [
  {
    path: '',
    component: ZellePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZellePageRoutingModule {}
