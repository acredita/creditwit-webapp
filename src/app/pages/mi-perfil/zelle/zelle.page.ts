import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavController, LoadingController } from "@ionic/angular";
import { StorageService } from "src/app/services/storage.service";
import { BusinessService } from "src/app/services/business.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-zelle",
  templateUrl: "./zelle.page.html",
  styleUrls: ["./zelle.page.scss"]
})
export class ZellePage implements OnInit {
  cuentaForm: FormGroup;
  keyboard: boolean;
  loading: HTMLIonLoadingElement;
  nameInvalid: boolean;
  emailInvalid: boolean;
  cuenta: any;
  emailFormat: boolean;
  type: string = "Correo electrónico";
  typeBool = true;

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private storage: StorageService,
    private businessService: BusinessService
  ) {}

  ngOnInit() {
    if (this.storage.getCuenta()) {
      this.cuenta = JSON.parse(this.storage.getCuenta());
      console.log(this.cuenta.detalle.correo);
      console.log(String(this.cuenta.detalle.correo).includes("@"));
      if (String(this.cuenta.detalle.correo).includes("@")) {
        console.log("correo");
        this.typeBool = true;
        this.cuentaForm = this.fb.group({
          name: [this.cuenta.detalle.nombre, [Validators.required]],
          type: ["Correo electrónico", [Validators.required]],
          email: [this.cuenta.detalle.correo, [Validators.required]]
        });
      } else {
        this.typeBool = false;
        this.cuentaForm = this.fb.group({
          name: [this.cuenta.detalle.nombre, [Validators.required]],
          type: ["Número de teléfono", [Validators.required]],
          email: [this.cuenta.detalle.correo, [Validators.required]]
        });
      }
    } else {
      this.cuentaForm = this.fb.group({
        name: ["", [Validators.required]],
        type: ["Correo electrónico", [Validators.required]],
        email: ["", [Validators.required]]
      });
    }
    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  goBack() {
    if (this.storage.getCuenta()) {
      this.navCtrl.navigateBack("/tabs/perfil/datos-bancarios");
    } else {
      this.navCtrl.navigateBack("/tabs/perfil/agregar-datos");
    }
  }

  get errorControl() {
    return this.cuentaForm.controls;
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  onInput(event) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  changeType() {
    console.log(this.type);
    if (this.type == "Correo electrónico") {
      this.type = "Número de teléfono";
      this.typeBool = false;
    } else {
      this.type = "Correo electrónico";
      this.typeBool = true;
    }
    this.cuentaForm.patchValue({ email: "" });
  }

  async onContinue() {
    let regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    const form = this.cuentaForm.value;
    this.nameInvalid = false;
    this.emailInvalid = false;
    this.emailFormat = false;

    if (form.name == "") {
      this.nameInvalid = true;
    }

    if (form.email == "") {
      this.emailInvalid = true;
    }

    if (this.typeBool && !regex.test(form.email) && form.email != "") {
      this.emailFormat = true;
      return;
    }

    if (!this.typeBool && form.email.length != 14) {
      this.emailInvalid = true;
      return;
    }

    console.log(this.cuentaForm.valid);

    if (this.cuentaForm.valid) {
      this.presentLoading("Validando...");
      let correo = form.email;
      if (!this.typeBool) {
        correo = correo.replace("(", "").replace(")", "").replace(/\s/g, "");
      }
      if (this.storage.getCuenta()) {
        let data = {
          id: this.cuenta._id,
          currency: "usd",
          metodo: "Zelle",
          detalle: {
            nombre: form.name,
            correo: correo
          }
        };
        (await this.businessService.modifycuentasBancarias(data)).subscribe(
          (resp: any) => {
            console.log(resp);
            let cuenta = {
              titulo: "Zelle modificado",
              line1: form.name,
              line2: form.email
            };
            this.storage.setCuentaExito(JSON.stringify(cuenta));
            this.navCtrl.navigateBack("/tabs/perfil/exito");
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          },
          (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          }
        );
      } else {
        let cuentas = JSON.parse(this.storage.getCuentas());
        let data = {
          currency: "usd",
          metodo: "Zelle",
          detalle: {
            nombre: form.name,
            correo: correo
          }
        };
        let repetido = false;
        cuentas.forEach((cuenta) => {
          if (cuenta.metodo == "Zelle") {
            if (correo == cuenta.detalle.correo) {
              Swal.fire({
                confirmButtonColor: "#E7A016",
                text: "Este método de pago ya se encuentra registrado",
                heightAuto: false,
                showCloseButton: true
              });
              repetido = true;
              return;
            }
          }
        });
        if (!repetido) {
          (await this.businessService.addcuentasBancarias(data)).subscribe(
            (resp: any) => {
              console.log(resp);
              let userData = JSON.parse(this.storage.getUserData());
              userData.payMethods = resp.payMethods;
              this.storage.setUserData(JSON.stringify(userData));
              let cuenta = {
                titulo: "Zelle agregado",
                line1: form.name,
                line2: form.email
              };
              this.storage.setCuentaExito(JSON.stringify(cuenta));
              this.navCtrl.navigateBack("/tabs/perfil/exito");
              setTimeout(() => {
                this.loading.dismiss();
              }, 500);
            },
            (err) => {
              console.log(err);
              setTimeout(() => {
                this.loading.dismiss();
              }, 500);
            }
          );
        } else {
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
        }
      }
    }
  }
}
