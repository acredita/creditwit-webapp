import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavController, LoadingController } from "@ionic/angular";
import { StorageService } from "src/app/services/storage.service";
import { BusinessService } from "src/app/services/business.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-cuenta-bancaria",
  templateUrl: "./cuenta-bancaria.page.html",
  styleUrls: ["./cuenta-bancaria.page.scss"]
})
export class CuentaBancariaPage implements OnInit {
  cuentaForm: FormGroup;
  keyboard: boolean;
  loading: HTMLIonLoadingElement;
  nameInvalid: boolean;
  idInvalid: boolean;
  bankInvalid: boolean;
  typeInvalid: boolean;
  numberInvalid: boolean;
  cuenta: any;
  bank: any = "";
  prefijo: any;
  bankPrefijo: boolean;
  fixedID: string;
  cuentas = [
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e316"
      },
      nombre: "Banesco",
      prefijo: "0134"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e31f"
      },
      nombre: "Provincial",
      prefijo: "0108"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e329"
      },
      nombre: "Venezuela",
      prefijo: "0102"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e321"
      },
      nombre: "Mercantil",
      prefijo: "0105"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e333"
      },
      nombre: "Banco Bicentenario",
      prefijo: "0175"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e32e"
      },
      nombre: "BNC",
      prefijo: "0191"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e317"
      },
      nombre: "BFC",
      prefijo: "0151"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e318"
      },
      nombre: "Activo",
      prefijo: "0171"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e31d"
      },
      nombre: "Banplus",
      prefijo: "0174"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e32b"
      },
      nombre: "BOD",
      prefijo: "0116"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e32f"
      },
      nombre: "Bancaribe",
      prefijo: "0114"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e330"
      },
      nombre: "Exterior",
      prefijo: "0115"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e327"
      },
      nombre: "Banco Del Tesoro",
      prefijo: "0163"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e31e"
      },
      nombre: "DelSur",
      prefijo: "0157"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e320"
      },
      nombre: "100% Banco",
      prefijo: "0156"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e322"
      },
      nombre: "Sofitasa",
      prefijo: "0137"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e325"
      },
      nombre: "Caroní",
      prefijo: "0128"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e332"
      },
      nombre: "Banco Plaza",
      prefijo: "0138"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e31a"
      },
      nombre: "Internacional de Desarrollo",
      prefijo: "0173"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e31b"
      },
      nombre: "Novo Banco",
      prefijo: "0176"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e31c"
      },
      nombre: "Banco de la FANB",
      prefijo: "0177"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e323"
      },
      nombre: "Bancamiga",
      prefijo: "0172"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e324"
      },
      nombre: "Banco Venezolano de Crédito",
      prefijo: "0104"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e326"
      },
      nombre: "Gente Emprendedora",
      prefijo: "0146"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e328"
      },
      nombre: "Mi Banco",
      prefijo: "0169"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e32c"
      },
      nombre: "Citibank",
      prefijo: "0190"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e32d"
      },
      nombre: "Bancrecer",
      prefijo: "0168"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e331"
      },
      nombre: "Banco Agricola de venezuela",
      prefijo: "0166"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e334"
      },
      nombre: "Instituto Municipal de Crédito",
      prefijo: "0601"
    },
    {
      _id: {
        $oid: "5f7254f5ddf1d9d0fd92e32a"
      },
      nombre: "Banco Central de Venezuela",
      prefijo: "0001"
    }
  ];

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private storage: StorageService,
    private businessService: BusinessService
  ) {}

  async ngOnInit() {
    if (this.storage.getCuenta()) {
      this.cuenta = JSON.parse(this.storage.getCuenta());
      this.cuentaForm = this.fb.group({
        name: [this.cuenta.detalle.nombre, [Validators.required]],
        letter: [
          this.cuenta.detalle.cedula.substring(0, 1),
          [Validators.required]
        ],
        id: [
          this.cuenta.detalle.cedula.substring(
            1,
            this.cuenta.detalle.cedula.length
          ),
          [Validators.required]
        ],
        bank: [this.bank, [Validators.required]],
        type: [this.cuenta.detalle.tipoCuenta, [Validators.required]],
        number: [this.cuenta.detalle.numero, [Validators.required]]
      });
      this.bank = this.cuenta.detalle.banco;
      this.fixedID = Number(
        this.cuenta.detalle.cedula.substring(
          1,
          this.cuenta.detalle.cedula.length
        )
      ).toLocaleString("de-DE");
      this.prefijo = this.cuenta.detalle.numero.substring(0, 4);
    } else {
      this.cuentaForm = this.fb.group({
        name: ["", [Validators.required]],
        letter: ["V", [Validators.required]],
        id: ["", [Validators.required]],
        bank: ["", [Validators.required]],
        type: ["", [Validators.required]],
        number: ["", [Validators.required]]
      });
    }

    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  async onContinue() {
    const form = this.cuentaForm.value;
    this.nameInvalid = false;
    this.idInvalid = false;
    this.bankInvalid = false;
    this.typeInvalid = false;
    this.numberInvalid = false;
    this.bankPrefijo = false;
    let splitArray: [] = form.name.split(" ");

    if (form.name == "") {
      this.nameInvalid = true;
    }

    if (form.letter != "J" && splitArray.length) {
      this.nameInvalid = true;
    }

    if (form.id == "") {
      this.idInvalid = true;
    }

    if (form.bank == "") {
      this.bankInvalid = true;
    }

    if (form.type == "") {
      this.typeInvalid = true;
    }

    if (form.number == "") {
      this.numberInvalid = true;
    }

    if (form.number.length != 20) {
      this.numberInvalid = true;
      return;
    }

    if (
      this.prefijo != form.number.substring(0, 4) &&
      form.number != "" &&
      form.bank != ""
    ) {
      this.bankPrefijo = true;
      return;
    }

    if (this.cuentaForm.valid) {
      this.presentLoading("Validando...");
      if (this.storage.getCuenta()) {
        let data = {
          id: this.cuenta._id,
          currency: "bs",
          metodo: "Transferencia",
          detalle: {
            nombre: form.name,
            cedula:
              form.letter +
              form.id
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", ""),
            banco: form.bank,
            tipoCuenta: form.type,
            numero: form.number
          }
        };
        console.log(data);
        (await this.businessService.modifycuentasBancarias(data)).subscribe(
          (resp: any) => {
            console.log(resp);
            let cuenta = {
              titulo: "Cuenta modificada",
              line1: form.name,
              line2: form.letter + form.id,
              line3: form.bank + " - " + form.type,
              line4: form.number
            };
            this.storage.setCuentaExito(JSON.stringify(cuenta));
            this.navCtrl.navigateBack("/tabs/perfil/exito");
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          },
          (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          }
        );
      } else {
        console.log(JSON.parse(this.storage.getCuentas()));
        let cuentas = JSON.parse(this.storage.getCuentas());
        let data = {
          currency: "bs",
          metodo: "Transferencia",
          detalle: {
            nombre: form.name,
            cedula:
              form.letter +
              form.id
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", ""),
            banco: form.bank,
            tipoCuenta: form.type,
            numero: form.number
          }
        };
        let repetido = false;
        cuentas.forEach((cuenta) => {
          if (cuenta.metodo == "Transferencia") {
            if (form.number == cuenta.detalle.numero) {
              Swal.fire({
                confirmButtonColor: "#E7A016",
                text: "Este método de pago ya se encuentra registrado",
                heightAuto: false,
                showCloseButton: true
              });
              repetido = true;
              return;
            }
          }
        });
        if (!repetido) {
          (await this.businessService.addcuentasBancarias(data)).subscribe(
            (resp: any) => {
              console.log(resp);
              let userData = JSON.parse(this.storage.getUserData());
              userData.payMethods = resp.payMethods;
              this.storage.setUserData(JSON.stringify(userData));
              let cuenta = {
                titulo: "Cuenta agregada",
                line1: form.name,
                line2:
                  form.letter +
                  Number(
                    form.id
                      .replace(",", "")
                      .replace(",", "")
                      .replace(",", "")
                      .replace(",", "")
                      .replace(".", "")
                      .replace(".", "")
                      .replace(".", "")
                      .replace(".", "")
                  ).toLocaleString("de-DE"),
                line3: form.bank + " - " + form.type,
                line4: form.number
              };
              this.storage.setCuentaExito(JSON.stringify(cuenta));
              this.navCtrl.navigateBack("/tabs/perfil/exito");
              setTimeout(() => {
                this.loading.dismiss();
              }, 500);
            },
            (err) => {
              console.log(err);
              setTimeout(() => {
                this.loading.dismiss();
              }, 500);
            }
          );
        } else {
          setTimeout(() => {
            this.loading.dismiss();
          }, 500);
        }
      }
    }
  }

  get errorControl() {
    return this.cuentaForm.controls;
  }

  goBack() {
    if (this.storage.getCuenta()) {
      this.navCtrl.navigateBack("/tabs/perfil/datos-bancarios");
    } else {
      this.navCtrl.navigateBack("/tabs/perfil/agregar-datos");
    }
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  onInput(event) {
    //console.log(event)
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  onChange() {
    console.log(this.bank);
    this.cuentas.forEach((cuenta) => {
      if (cuenta.nombre == this.bank) {
        this.prefijo = cuenta.prefijo;
        return;
      }
    });
    this.cuentaForm.patchValue({
      number: this.prefijo
    });
  }

  formatID(event: any) {
    //console.log(' ID ' + event.target.value);
    if (event.detail.value !== "") {
      let value = String(event.detail.value)
        .replace(".", "")
        .replace(".", "")
        .replace(".", "")
        .replace(",", "")
        .replace(",", "")
        .replace(",", "");
      this.fixedID = parseInt(value)
        .toString()
        .replace(/\./g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    //console.log('fixed ID ' + this.fixedID);
  }
}
