import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentaBancariaPage } from './cuenta-bancaria.page';

const routes: Routes = [
  {
    path: '',
    component: CuentaBancariaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentaBancariaPageRoutingModule {}
