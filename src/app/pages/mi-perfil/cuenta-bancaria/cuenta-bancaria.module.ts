import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentaBancariaPageRoutingModule } from './cuenta-bancaria-routing.module';

import { CuentaBancariaPage } from './cuenta-bancaria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CuentaBancariaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CuentaBancariaPage]
})
export class CuentaBancariaPageModule {}
