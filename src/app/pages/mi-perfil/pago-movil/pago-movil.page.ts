import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavController, LoadingController } from "@ionic/angular";
import { BusinessService } from "src/app/services/business.service";
import { StorageService } from "src/app/services/storage.service";

@Component({
  selector: "app-pago-movil",
  templateUrl: "./pago-movil.page.html",
  styleUrls: ["./pago-movil.page.scss"]
})
export class PagoMovilPage implements OnInit {
  cuentaForm: FormGroup;
  keyboard: boolean;
  loading: any;
  idInvalid: boolean;
  phoneInvalid: boolean;
  bankInvalid: boolean;
  cuenta: any;
  cuentas: any;
  bank: any;
  fixedID: string;

  constructor(
    public fb: FormBuilder,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private storage: StorageService,
    private businessService: BusinessService
  ) {}

  ngOnInit() {
    if (this.storage.getCuenta()) {
      this.cuenta = JSON.parse(this.storage.getCuenta());
      this.cuentaForm = this.fb.group({
        letter: [
          this.cuenta.detalle.cedula.substring(0, 1),
          [Validators.required]
        ],
        id: [
          this.cuenta.detalle.cedula
            .substring(1, this.cuenta.detalle.cedula.length)
            .toLocaleString("de-DE"),
          [Validators.required]
        ],
        phone: [
          this.cuenta.detalle.numero.replace("+58", "0"),
          [Validators.required]
        ],
        bank: [this.cuenta.detalle.banco, [Validators.required]]
      });
      this.bank = this.cuenta.detalle.banco;
      this.fixedID = Number(
        this.cuenta.detalle.cedula.substring(
          1,
          this.cuenta.detalle.cedula.length
        )
      ).toLocaleString("de-DE");
      this.businessService.getCuentas().subscribe((resp: any) => {
        this.cuentas = resp;
        this.bank = this.cuenta.detalle.banco;
      });
    } else {
      this.cuentaForm = this.fb.group({
        letter: ["V", [Validators.required]],
        id: ["", [Validators.required]],
        phone: ["", [Validators.required]],
        bank: ["", [Validators.required]]
      });
      this.businessService.getCuentas().subscribe((resp: any) => {
        this.cuentas = resp;
      });
    }
    window.addEventListener("keyboardDidHide", () => {
      this.keyboard = true;
    });
    window.addEventListener("keyboardDidShow", (event) => {
      this.keyboard = false;
    });
  }

  goBack() {
    if (this.storage.getCuenta()) {
      this.navCtrl.navigateBack("/tabs/perfil/datos-bancarios");
    } else {
      this.navCtrl.navigateBack("/tabs/perfil/agregar-datos");
    }
  }

  get errorControl() {
    return this.cuentaForm.controls;
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  onInput(event) {
    let regex = /[0-9]/;

    if (!regex.test(event.key)) {
      if (event.key !== "Backspace") {
        event.preventDefault();
      }
    }
  }

  async onContinue() {
    const form = this.cuentaForm.value;
    this.idInvalid = false;
    this.bankInvalid = false;
    this.phoneInvalid = false;

    if (form.id == "") {
      this.idInvalid = true;
    }

    if (form.bank == "") {
      this.bankInvalid = true;
    }

    if (form.phone == "") {
      this.phoneInvalid = true;
    }

    if (this.cuentaForm.valid) {
      this.presentLoading("Validando...");
      if (this.storage.getCuenta()) {
        let data = {
          id: this.cuenta._id,
          currency: "bs",
          metodo: "PagoMovil",
          detalle: {
            cedula:
              form.letter +
              form.id
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", ""),
            banco: form.bank,
            numero: `+58${form.phone
              .replace(" ", "")
              .replace(" ", "")
              .replace("(", "")
              .replace(")", "")
              .replace("-", "")
              .substring(1, form.phone.length - 1)}`
          }
        };
        (await this.businessService.modifycuentasBancarias(data)).subscribe(
          (resp: any) => {
            console.log(resp);
            let cuenta = {
              titulo: "Pago móvil modificado",
              line1:
                form.letter +
                Number(
                  form.id
                    .replace(",", "")
                    .replace(",", "")
                    .replace(",", "")
                    .replace(",", "")
                    .replace(".", "")
                    .replace(".", "")
                    .replace(".", "")
                    .replace(".", "")
                ).toLocaleString("de-DE"),
              line2: form.bank,
              line3: form.phone
            };
            this.storage.setCuentaExito(JSON.stringify(cuenta));
            this.navCtrl.navigateBack("/tabs/perfil/exito");
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          },
          (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          }
        );
      } else {
        let data = {
          currency: "bs",
          metodo: "PagoMovil",
          detalle: {
            cedula:
              form.letter +
              form.id
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(",", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", "")
                .replace(".", ""),
            banco: form.bank,
            numero: `+58${form.phone
              .replace(" ", "")
              .replace(" ", "")
              .replace("(", "")
              .replace(")", "")
              .replace("-", "")
              .substring(1, form.phone.length - 1)}`
          }
        };
        (await this.businessService.addcuentasBancarias(data)).subscribe(
          (resp: any) => {
            console.log(resp);
            let userData = JSON.parse(this.storage.getUserData());
            userData.payMethods = resp.payMethods;
            this.storage.setUserData(JSON.stringify(userData));
            let cuenta = {
              titulo: "Pago móvil agregado",
              line1:
                form.letter +
                Number(
                  form.id
                    .replace(",", "")
                    .replace(",", "")
                    .replace(",", "")
                    .replace(",", "")
                    .replace(".", "")
                    .replace(".", "")
                    .replace(".", "")
                    .replace(".", "")
                ).toLocaleString("de-DE"),
              line2: form.bank,
              line3: form.phone
            };
            this.storage.setCuentaExito(JSON.stringify(cuenta));
            this.navCtrl.navigateBack("/tabs/perfil/exito");
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          },
          (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500);
          }
        );
      }
    }
  }

  formatID(event: any) {
    //console.log(' ID ' + event.target.value);
    if (event.detail.value !== "") {
      let value = String(event.detail.value)
        .replace(".", "")
        .replace(".", "")
        .replace(".", "")
        .replace(",", "")
        .replace(",", "")
        .replace(",", "");
      this.fixedID = parseInt(value)
        .toString()
        .replace(/\./g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    //console.log('fixed ID ' + this.fixedID);
  }
}
