import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagoMovilPageRoutingModule } from './pago-movil-routing.module';

import { PagoMovilPage } from './pago-movil.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagoMovilPageRoutingModule,
    ReactiveFormsModule,
    BrMaskerModule
  ],
  declarations: [PagoMovilPage]
})
export class PagoMovilPageModule {}
