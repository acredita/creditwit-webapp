import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagoMovilPage } from './pago-movil.page';

const routes: Routes = [
  {
    path: '',
    component: PagoMovilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagoMovilPageRoutingModule {}
