import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, NavController, Platform } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { BusinessService } from 'src/app/services/business.service';
import Swal from 'sweetalert2';
import { PushService } from '../../../services/push.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  loading: any;
  apellidos: string;
  nombres: string;
  correo: string;
  telefono: string;
  rating: string;
  cuentas = [];
  numCuentas: string;
  cedula: string;
  numeroCalificaciones: number;
  push: boolean;
  cordova: boolean;

  constructor(
    private toastController: ToastController,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private navCtrl: NavController,
    private platform: Platform,
    private pushService: PushService) { }

  ngOnInit() {
    if (this.platform.is('cordova')) {
      this.cordova = true;
    }
    else {
      this.cordova = false;
    }
    let userData = JSON.parse(this.storage.getUserData());
    this.push = userData.config.pushNotifications;
    this.apellidos = userData.apellidos;
    this.nombres = userData.nombres;
    this.correo = userData.correo;
    this.telefono = userData.telefono.replace('+58', '0');
    this.telefono = `${this.telefono.substring(0, 4)} ${this.telefono.substring(4, 7)} ${this.telefono.substring(7, 11)}`;
    this.rating = userData.rating.puntuacion;
    this.cuentas = userData.payMethods;
    this.numCuentas = this.cuentas.length > 1 ? ' Cuentas' : ' Cuenta';
    this.numeroCalificaciones = userData.rating.numeroCalificaciones;
    this.cedula = `${userData.cedula.substring(0, 1)} ${Number(userData.cedula.substring(1, userData.cedula.length)).toLocaleString('de-DE')}`;
    this.storage.setCuentas(JSON.stringify(this.cuentas));
    console.log(userData)
  }

  async ionViewWillEnter() {
    if ( JSON.parse(this.storage.getContext()).enviarCobro){
      let data = {
        enviarCobro: false
      }
      this.storage.setContext( JSON.stringify(data) );
      this.navCtrl.navigateForward('/tabs/enviar-cobro');
    }
    (await this.businessService.getPerfil())
      .subscribe((resp: any) => {
        console.log(resp);
        this.push = resp.config.pushNotifications;
        this.apellidos = resp.apellidos;
        this.nombres = resp.nombres;
        this.correo = resp.correo; 
        this.telefono = resp.telefono.replace('+58', '0');
        this.telefono = `${this.telefono.substring(0, 4)} ${this.telefono.substring(4, 7)} ${this.telefono.substring(7, 11)}`;
        this.rating = resp.rating.puntuacion;
        this.cuentas = resp.payMethods;
        this.numCuentas = this.cuentas.length > 1 ? ' Cuentas' : ' Cuenta';
        this.numeroCalificaciones = resp.rating.numeroCalificaciones;
        this.cedula = `${resp.cedula.substring(0, 1)} ${Number(resp.cedula.substring(1, resp.cedula.length)).toLocaleString('de-DE')}`;
        this.storage.setCuentas(JSON.stringify(this.cuentas));
        const ratingHistoryData = resp.rating.calificaciones;
        this.storage.setRatingHistory(JSON.stringify(ratingHistoryData));
      }, (err) => {
        console.log(err);
      });
  }

  async clipboard() {
    let copyText = "info@creditwit.app";
    var textArea = document.getElementById('email') as HTMLInputElement;

    textArea.select();
    textArea.setSelectionRange(0, 99999);
    /* Copy the text inside the text field */
    document.execCommand("copy");

    /* Alert the copied text */
    const toast = await this.toastController.create({
      message: 'Correo copiado',
      duration: 1500
    });
    toast.present();
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingCtrl.create({
      message
      // duration: 2000
    });
    return this.loading.present();
  }

  cerrarSesion() {
    Swal.fire({
      imageUrl: './assets/images/icons/warning.svg',
      imageHeight: 80,
      imageWidth: 80,
      imageAlt: 'A tall image',
      text: '¿Estás seguro de que deseas cerrar sesión?',
      showCancelButton: true,
      showCloseButton: true,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Si',
      cancelButtonColor: '#E7A016',
      cancelButtonText: 'No',
      heightAuto: false,
    }).then(async (result) => {
      if (result.value) {
        if (this.platform.is('cordova')) {
          let data = {
            fireToken: '-'
          };
          (await this.businessService.setTokenPush(data))
            .subscribe((resp: any) => {
              console.log(resp);
            }, (err) => {
              console.log(err);
              localStorage.clear();
              this.navCtrl.navigateRoot('/login');
            });
        }
        localStorage.clear();
        this.navCtrl.navigateRoot('/login');
      }
    })
  }

  showRatingHistory() {
    this.navCtrl.navigateForward('/tabs/perfil/historial-evaluaciones');
  }

  async setPush() {
    (await this.businessService.activePush())
      .subscribe(async (resp: any) => {
        console.log(resp)
        if (resp.pushNotifications) {
          this.pushService.configuracionInicial();
        }
        else {
          let data = {
            fireToken: '-'
          };
          (await this.businessService.setTokenPush(data))
            .subscribe((resp: any) => {
              console.log(resp);
            }, (err) => {
              console.log(err);
            });
        }
      }, (err) => {
        console.log(err);
      });
  }

}
