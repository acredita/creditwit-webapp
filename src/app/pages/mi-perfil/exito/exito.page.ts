import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-exito',
  templateUrl: './exito.page.html',
  styleUrls: ['./exito.page.scss'],
})
export class ExitoPage implements OnInit {

  cuenta: any;

  constructor( private navCtrl: NavController,
    private storage: StorageService, ) { }

  ngOnInit() {
    this.cuenta = JSON.parse( this.storage.getCuentaExito() );
  }

  ionViewWillEnter(){
    this.cuenta = JSON.parse( this.storage.getCuentaExito() );
  }

  goBack(){
    this.navCtrl.back();
  }

  continue(){
    if ( JSON.parse(this.storage.getContext()).enviarCobro){
      this.navCtrl.navigateBack('/tabs/perfil');
    }
    else{
      this.navCtrl.navigateBack('/tabs/perfil/datos-bancarios');
    }
  }

}
