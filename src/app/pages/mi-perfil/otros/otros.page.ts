import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, LoadingController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { BusinessService } from 'src/app/services/business.service';

@Component({
  selector: 'app-otros',
  templateUrl: './otros.page.html',
  styleUrls: ['./otros.page.scss'],
})
export class OtrosPage implements OnInit {

  cuentaForm: FormGroup;
  keyboard: boolean;
  titleInvalid: boolean;
  loading: any;
  cuenta: any;

  constructor( public fb: FormBuilder,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private storage: StorageService,
    private businessService: BusinessService  ) { }

    ngOnInit() {
      if ( this.storage.getCuenta() ){
        this.cuenta = JSON.parse(this.storage.getCuenta());
        this.cuentaForm = this.fb.group({
          title: [this.cuenta.detalle.titulo, [Validators.required]],
          comment: [this.cuenta.detalle.extra, [Validators.required]]
        });
      }
      else{
        this.cuentaForm = this.fb.group({
          title: ['', [Validators.required]],
          comment: ['']
        });
      }
      window.addEventListener('keyboardDidHide', () => {
        this.keyboard = true;
      });
      window.addEventListener('keyboardDidShow', (event) => {
        this.keyboard = false;
      });
    }

    get errorControl() {
      return this.cuentaForm.controls;
    }
  
    async presentLoading( message: string ) {
      this.loading = await this.loadingCtrl.create({
        message
      });
      return this.loading.present();
    }
  
    async onContinue(){
      const form = this.cuentaForm.value;
      this.titleInvalid = false;
  
      if (form.title == ''){
        this.titleInvalid = true;
      }
  
      if ( this.cuentaForm.valid ){
        this.presentLoading( 'Validando...' );
        if ( this.storage.getCuenta() ){
          let data = {
            id: this.cuenta._id,
            currency: 'usd',
            metodo: 'Otro',
            detalle: {
              titulo: form.title,
              extra: form.comment
            }
          };
          (await this.businessService.modifycuentasBancarias(data))
          .subscribe( (resp : any) => {
            console.log(resp)
            let cuenta = {
              titulo: 'Datos modificados',
              line1: form.title,
              line2: form.comment,
            }
            this.storage.setCuentaExito( JSON.stringify( cuenta ) );
            this.navCtrl.navigateBack('/tabs/perfil/exito');
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          }, (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          });
        }
        else{
          let data = {
            currency: 'usd',
            metodo: 'Otro',
            detalle: {
              titulo: form.title,
              extra: form.comment
            }
          };
          (await this.businessService.addcuentasBancarias(data))
          .subscribe( (resp : any) => {
            console.log(resp)
            let userData = JSON.parse(this.storage.getUserData());
            userData.payMethods = resp.payMethods;
            this.storage.setUserData( JSON.stringify( userData ) );
            let cuenta = {
              titulo: 'Datos agregados',
              line1: form.title,
              line2: form.comment,
            }
            this.storage.setCuentaExito( JSON.stringify( cuenta ) );
            this.navCtrl.navigateBack('/tabs/perfil/exito');
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          }, (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          });
        }
      }
    }
  
    goBack(){
      if ( this.storage.getCuenta() ){
        this.navCtrl.navigateBack('/tabs/perfil/datos-bancarios');
      }
      else{
        this.navCtrl.navigateBack('/tabs/perfil/agregar-datos');
      }
    }

}
