import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, Platform } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { BusinessService } from 'src/app/services/business.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-datos-bancarios',
  templateUrl: './datos-bancarios.page.html',
  styleUrls: ['./datos-bancarios.page.scss'],
})
export class DatosBancariosPage implements OnInit {
  cuentas: any;
  editar: boolean = false;
  loading: HTMLIonLoadingElement;

  constructor( private navCtrl: NavController,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private businessService: BusinessService,
    private platform: Platform ) { }

  ngOnInit() {
    if ( this.platform.is('ios') && this.platform.is('cordova') ){
      document.getElementById('content-principal-datos-bancarios').classList.add('content-principal-ios');
      document.getElementById('content-list-datos-bancarios').classList.add('content-list-ios');
    }
    this.cuentas = JSON.parse(this.storage.getCuentas());
    this.orderCuentas();
  }

  async ionViewWillEnter(){
    this.storage.removeCuenta();
    (await this.businessService.getPerfil())
      .subscribe((resp: any) => {
        console.log(resp);
        this.cuentas = resp.payMethods;
        this.storage.setCuentas(JSON.stringify(this.cuentas));
        this.orderCuentas();
      }, (err) => {
        console.log(err);
      });
  }

  goBack(){
    this.navCtrl.navigateBack('/tabs/perfil');
  }

  editarCuenta(){
    this.editar = !this.editar;
  }

  modificarCuenta( cuenta ){
    if (this.editar){
      this.storage.setCuenta( JSON.stringify( cuenta ) );
      if ( cuenta.metodo === 'Transferencia' ){
        this.navCtrl.navigateForward( '/tabs/perfil/cuenta-bancaria' );
      }
      else if ( cuenta.metodo === 'Zelle' ){
        this.navCtrl.navigateForward( '/tabs/perfil/zelle' );
      }
      else if ( cuenta.metodo === 'PagoMovil' ){
        this.navCtrl.navigateForward( '/tabs/perfil/pago-movil' );
      }
      else if ( cuenta.metodo === 'Otro' ){
        this.navCtrl.navigateForward( '/tabs/perfil/otros' );
      }
    }
  }

  eliminarCuenta( cuenta ){
    Swal.fire({
      imageUrl: './assets/images/icons/warning.svg',
      imageHeight: 80,
      imageWidth: 80,
      imageAlt: 'A tall image',
      text: '¿Estás seguro de que deseas eliminar estos datos?',
      showCancelButton: false,
      showCloseButton: true,
      confirmButtonColor: '#E7A016',
      confirmButtonText: 'Eliminar',
      heightAuto: false,
    }).then(async (result) => {
      if (result.value) {
        this.presentLoading( 'Eliminando...' );
        (await this.businessService.deletecuentasBancarias(cuenta._id))
          .subscribe( (resp : any) => {
            console.log(resp.payMethods)
            this.cuentas = resp.payMethods;
            let userData = JSON.parse(this.storage.getUserData());
            userData.payMethods = resp.payMethods;
            this.storage.setUserData( JSON.stringify( userData ) );
            this.orderCuentas();
            this.storage.setCuentas( JSON.stringify( resp.payMethods ) );
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          }, (err) => {
            console.log(err);
            setTimeout(() => {
              this.loading.dismiss();
            }, 500 );
          });
      }
    })
  }

  async presentLoading( message: string ) {
    this.loading = await this.loadingCtrl.create({
      message
    });
    return this.loading.present();
  }

  orderCuentas(){
    if ( this.cuentas.length > 0 ){
      this.cuentas.forEach(cuenta => {
        if ( cuenta.metodo === 'Transferencia' ){
          cuenta.titulo = cuenta.detalle.banco;
          cuenta.subtitulo = cuenta.detalle.tipoCuenta;
          cuenta.info = `${cuenta.detalle.numero.substring(0,4)} **** **** **** ${cuenta.detalle.numero.substring(cuenta.detalle.numero.length - 4, cuenta.detalle.numero.length)}`;
        }
        else if ( cuenta.metodo === 'Zelle' ){
          cuenta.titulo = cuenta.metodo;
          if ( cuenta.detalle.correo.includes('@') ){
            cuenta.subtitulo =  'Correo eletrónico';
            cuenta.info = cuenta.detalle.correo;
          }
          else{
            cuenta.subtitulo =  'Número de teléfono';
            cuenta.info = `(${cuenta.detalle.correo.substring(0,3)}) ${cuenta.detalle.correo.substring(3,6)} ${cuenta.detalle.correo.substring(6,10)}`;
          }
        }
        else if ( cuenta.metodo === 'Efectivo' ){
          cuenta.titulo = 'Efectivo';
          cuenta.subtitulo = cuenta.metodo;
          cuenta.info = 'Pagos presenciales';
        }
        else if ( cuenta.metodo === 'PagoMovil' ){
          cuenta.titulo = cuenta.detalle.banco;
          cuenta.subtitulo = 'Pago móvil';
          cuenta.info = cuenta.detalle.numero.replace('+58', '0');
          cuenta.info = `${cuenta.info.substring(0,4)} ${cuenta.info.substring(4,7)} ${cuenta.info.substring(7,11)}`;
        }
        else if ( cuenta.metodo === 'Otro' ){
          cuenta.titulo = 'Otros';
          cuenta.subtitulo = cuenta.detalle.titulo;
          cuenta.info = cuenta.detalle.extra;
        }
        else if ( cuenta.metodo === 'Efectivo' ){
          cuenta.titulo = 'Efectivo';
          cuenta.subtitulo = 'Efectivo';
          cuenta.info = 'Pagos presenciales';
        }
      });
    }
  }

}
